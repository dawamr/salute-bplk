<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class CourseController extends Controller
{
    public function index()
    {
        if($this->checkPermission('course.view')) abort(404);

        return view('admin.course');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('course.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
            'description' => $request->description,
        ];

        $insert = Course::create($data);


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan mapel']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan mapel']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('course.update')) abort(404);

        $course = Course::find($request->id);

        return response()->json(['status' => 'success', 'data' => $course]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('course.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $course = Course::find($request->id);

        $name = $request->name;
        $description = $request->description;

        $data = [
            'name' => $name,
            'description' => $description,
        ];

        $update = $course->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah mapel']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah mapel']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('course.delete')) abort(404);

        $destroy = Course::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus mapel']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus mapel']);
    }

    public function data()
    {
        if($this->checkPermission('course.view')) abort(404);

        $courses = Course::all();

        return DataTables::of($courses)
                    ->addColumn('action', function($course) {
                        $action = "";

                        if(auth()->user()->can('course.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Mapel' data-id='{$course->id}' onclick='getUpdateCourse(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('course.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Mapel' data-id='{$course->id}' onclick='deleteCourse(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function list(Request $request)
    {
        $term = $request->data['term'];

        $search = Course::where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    public function listQuestion()
    {
        $courses = Course::all();

        $html = '';

        if($courses->count() == 0){
            $link = route('admin.course');

            $html .= '<div class="col-12">';
            $html .= '<div class="card">';
            $html .= '<div class="card-body">';
            $html .= '<div class="empty-state" data-height="400">';
            $html .= '<div class="empty-state-icon">';
            $html .= '<i class="fas fa-question"></i>';
            $html .= '</div>';
            $html .= '<h2>Kami tidak menemukan data apapun</h2>';
            $html .= '<p class="lead">';
            $html .= 'Anda harus menambahkan mapel terlebih dahulu setidaknya satu';
            $html .= '</p>';
            $html .= '<a href="'.$link.'" class="btn btn-primary mt-4">Buat Mapel</a>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';

            return $html;
        }

        foreach($courses as $course){
            $link = route('admin.question.show', ['course' => $course]);

            $html .= '<div class="col-12 col-lg-4 col-md-4 col-sm-12" data-link="'.$link.'" onclick="redirectDetail(this)">';
            $html .= '<div class="card hoverable-shadow">';
            $html .= '<div class="card-body">';
            $html .= '<div class="empty-state">';
            $html .= '<h2 class="m-0">'.$course->name.'</h2>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }

        return $html;
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
