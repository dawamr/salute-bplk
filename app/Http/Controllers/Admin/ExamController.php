<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ExamController extends Controller
{
    public function index()
    {
        if($this->checkPermission('exam.view')) abort(404);

        return view('admin.exam');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('exam.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        DB::beginTransaction();
        try{
            $course = Course::find($request->course_id);

            $insert = $course->exams()->create($data);
            // $insert = Program::create($data);


            DB::commit();
        }catch(Exception $e){
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => $e->getMessage()]);
        }


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan materi']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan materi']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('exam.update')) abort(404);

        $exam = Exam::find($request->id);
        $course = $exam->course;

        return response()->json(['status' => 'success', 'data' => ['exam' => $exam, 'course' => $course]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('exam.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $exam = Exam::find($request->id);

        $name = $request->name;

        $data = [
            'name' => $name,
        ];

        $update = $exam->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah materi']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah materi']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('exam.delete')) abort(404);

        $destroy = Exam::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus materi']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus materi']);
    }

    public function data(Course $course)
    {
        if($this->checkPermission('exam.view')) abort(404);

        $exams = $course->exams;
        if($course->id == null) $exams = Exam::all();

        return DataTables::of($exams)
                    ->addColumn('course_name', function($exam) {
                        $course_name = "";

                        $course_name = $exam->course->name;

                        return $course_name;
                    })
                    ->addColumn('action', function($exam) {
                        $action = "";

                        if(auth()->user()->can('exam.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Materi' data-id='{$exam->id}' onclick='getUpdateExam(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('exam.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Materi' data-id='{$exam->id}' onclick='deleteExam(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function list(Request $request)
    {
        $term = $request->data['term'];

        $search = Exam::where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
