<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Instructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class InstructorController extends Controller
{
    public function index()
    {
        if($this->checkPermission('instructor.view')) abort(404);

        return view('admin.instructor');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('instructor.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        $insert = Instructor::create($data);


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan pengajar']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan pengajar']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('instructor.update')) abort(404);

        $instructor = Instructor::find($request->id);

        return response()->json(['status' => 'success', 'data' => $instructor]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('instructor.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $instructor = Instructor::find($request->id);

        $name = $request->name;

        $data = [
            'name' => $name,
        ];

        $update = $instructor->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah pengajar']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah pengajar']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('instructor.delete')) abort(404);

        $destroy = Instructor::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus pengajar']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus pengajar']);
    }

    public function data()
    {
        if($this->checkPermission('instructor.view')) abort(404);

        $instructors = Instructor::all();

        return DataTables::of($instructors)
                    ->addColumn('action', function($instructor) {
                        $action = "";

                        if(auth()->user()->can('instructor.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Pengajar' data-id='{$instructor->id}' onclick='getUpdateInstructor(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('instructor.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Pengajar' data-id='{$instructor->id}' onclick='deleteInstructor(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
