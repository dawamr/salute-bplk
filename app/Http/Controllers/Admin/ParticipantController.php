<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Participant;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ParticipantController extends Controller
{
    public function index()
    {
        if($this->checkPermission('participant.view')) abort(404);

        return view('admin.participant');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('participant.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ];

        $data_participant = [
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'education' => $request->education,
            'job' => $request->job,
        ];

        DB::beginTransaction();

        try{
            $insert = User::create($data);

            $default = Role::first();
            $role = Role::where('default_user', 1)->first();

            $role = $role ? $role->name : $default->name;

            $insert->assignRole($role);
            $insert->participant()->create($data_participant);

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan peserta']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('participant.update')) abort(404);

        $user = User::find($request->id);
        $participant = $user->participant;

        return response()->json(['status' => 'success', 'data' => ['user' => $user, 'participant' => $participant]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('participant.update')) abort(404);

        $validator = $this->validator($request->all(), 'update');

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $user = User::find($request->id);

        $username = $user->username;
        $name = $request->name;
        $email = $user->email;
        $password = $user->password;

        $gender = $request->gender;
        $date_of_birth = $request->date_of_birth;
        $education = $request->education;
        $job = $request->job;


        if($username != $request->username){
            $check_username = User::where('username', '=', $request->username)->get()->count();

            if($check_username != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Nama pengguna yang Anda masukkan sudah terdaftar']);
            }

            $username = $request->username;
        }

        if($email != $request->email){
            $check_email = User::where('email', '=', $request->email)->get()->count();

            if($check_email != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Email yang Anda masukkan sudah terdaftar']);
            }

            $email = $request->email;
        }

        if($request->password != null){
            $password = bcrypt($request->password);
        }

        $data = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        $data_participant = [
            'gender' => $gender,
            'date_of_birth' => $date_of_birth,
            'education' => $education,
            'job' => $job,
        ];

        DB::beginTransaction();

        try{
            $update = $user->update($data);

            $user->participant()->update($data_participant);

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah peserta']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('participant.delete')) abort(404);

        $user = User::find($request->id);

        if($user->profile_picture != null){
            Storage::delete($user->profile_picture);
        }

        $destroy = $user->delete();

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus peserta']);
    }

    public function data()
    {
        if($this->checkPermission('participant.view')) abort(404);

        $participants = Participant::all();

        return DataTables::of($participants)
                    ->addColumn('name', function($participant) {
                        $name = "";

                        $name = $participant->user->name;

                        return $name;
                    })
                    ->addColumn('username', function($participant) {
                        $username = "";

                        $username = $participant->user->username;

                        return $username;
                    })
                    ->addColumn('email', function($participant) {
                        $email = "";

                        $email = $participant->user->email;

                        return $email;
                    })
                    ->editColumn('gender', function($participant) {
                        $gender = "";

                        if($participant->gender == "L") $gender = "<span class='badge badge-primary'>Laki - laki</span>";
                        if($participant->gender == "P") $gender = "<span class='badge badge-warning'>Perempuan</span>";

                        return $gender;
                    })
                    ->addColumn('action', function($participant) {
                        $action = "";

                        // if(auth()->user()->can('participant.view')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-warning' tooltip='Detail Peserta' data-id='{$participant->user->id}' onclick='getDetailParticipant(this);'><i class='far fa-eye'></i></a>&nbsp;";
                        if(auth()->user()->can('participant.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Peserta' data-id='{$participant->user->id}' onclick='getUpdateParticipant(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('participant.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Peserta' data-id='{$participant->user->id}' onclick='deleteParticipant(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    protected function validator(array $data, $type = 'insert')
    {
        $username = 'unique:users,username';
        $email = 'unique:users,email';
        $password = ['string', 'min:8', 'max:191', 'confirmed'];

        if($type == 'update'){
            $username = '';
            $email = '';
            $password = [];
        }

        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:191','alpha_num', $username],
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', $email],
            'password' => $password,
            'gender' => ['required', 'string'],
            'date_of_birth' => ['required', 'date'],
            'education' => ['required', 'string'],
            'job' => ['required', 'string'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
