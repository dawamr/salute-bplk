<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin.profile');
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $user = User::find(auth()->user()->id);

        $username = $user->username;
        $name = $request->name;
        $email = $user->email;
        $password = $user->password;
        $profile_picture = $user->profile_picture;

        if($username != $request->username){
            $check_username = User::where('username', '=', $request->username)->get()->count();

            if($check_username != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Username yang Anda masukkan sudah terdaftar']);
            }

            $username = $request->username;
        }

        if($email != $request->email){
            $check_email = User::where('email', '=', $request->email)->get()->count();

            if($check_email != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Email yang Anda masukkan sudah terdaftar']);
            }

            $email = $request->email;
        }

        if($request->password != null){
            $password = bcrypt($request->password);
        }

        if($request->has('profile_picture')){
            Storage::delete($profile_picture);
            $profile_picture = $request->file('profile_picture')->store('uploads/user/profile');
        }

        if($request->null_profile_picture){
            Storage::delete($profile_picture);
            $profile_picture = null;
        }

        $data = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'profile_picture' => $profile_picture,
        ];

        $update = $user->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah profile Anda']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal mengubah profile Anda.']);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'email' => ':attribute tidak valid',
            'string' => ':attribute harus bertipe String',
            'min' => ':attribute minimal :min karakter',
            'max' => ':attribute maksimal :max karakter',
            'apha_num' => ':attribute tidak boleh ada spasi',
            'confirmed' => ':atrribute yang dimasukkan tidak sama',
            'file' => ':atrribute harus berupa file',
            'mimes' => ':atrribute harus bertipe :mimes',
        ];

        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:191','alpha_num'],
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191'],
            'password' => ['confirmed'],
            'profile_picture' => ['file', 'mimes:png,jpg,jpeg']
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
