<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\Vocational;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ProgramController extends Controller
{
    public function index()
    {
        if($this->checkPermission('program.view')) abort(404);

        return view('admin.program');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('program.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        DB::beginTransaction();
        try{
            $vocational = Vocational::find($request->vocational_id);

            $insert = $vocational->programs()->create($data);
            // $insert = Program::create($data);


            DB::commit();
        }catch(Exception $e){
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => $e->getMessage()]);
        }


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan program']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan program']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('program.update')) abort(404);

        $program = Program::find($request->id);
        $vocational = $program->vocational;

        return response()->json(['status' => 'success', 'data' => ['program' => $program, 'vocational' => $vocational]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('program.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $program = Program::find($request->id);

        $name = $request->name;

        $data = [
            'name' => $name,
        ];

        $update = $program->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah program']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah program']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('program.delete')) abort(404);

        $destroy = Program::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus program']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus program']);
    }

    public function data(Vocational $vocational)
    {
        if($this->checkPermission('program.view')) abort(404);

        $programs = $vocational->programs;
        if($vocational->id == null) $programs = Program::all();

        return DataTables::of($programs)
                    ->addColumn('vocational_name', function($program) {
                        $vocational_name = "";

                        $vocational_name = $program->vocational->name;

                        return $vocational_name;
                    })
                    ->addColumn('action', function($program) {
                        $action = "";

                        if(auth()->user()->can('program.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Program' data-id='{$program->id}' onclick='getUpdateProgram(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('program.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Program' data-id='{$program->id}' onclick='deleteProgram(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function list(Vocational $vocational, Request $request)
    {
        $term = $request->data['term'];

        $search = $vocational->programs()->where('name', 'like', '%'.$term.'%')->get();
        if($vocational->id == null) $search = Program::where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
