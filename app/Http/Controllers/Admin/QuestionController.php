<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Exam;
use App\Models\Question;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class QuestionController extends Controller
{
    public function index()
    {
        if($this->checkPermission('question.view')) abort(404);

        return view('admin.question.index');
    }

    public function show(Course $course)
    {
        if($this->checkPermission('question.view')) abort(404);

        return view('admin.question.show', compact('course'));
    }

    public function store(Request $request)
    {
        if($this->checkPermission('question.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'question_type' => $request->question_type,
            'question' => $request->question,
            'answer_1' => $request->answer_1,
            'answer_2' => $request->answer_2,
            'answer_3' => $request->answer_3,
            'answer_4' => $request->answer_4,
            'answer_5' => $request->answer_5,
            'point_1' => $request->point_1,
            'point_2' => $request->point_2,
            'point_3' => $request->point_3,
            'point_4' => $request->point_4,
            'point_5' => $request->point_5,
        ];

        if($request->question_group_id != null){
            $data['question_group_id'] = $request->question_group_id;
        }

        DB::beginTransaction();
        try{
            $exam = Exam::find($request->exam_id);

            $insert = $exam->questions()->create($data);

            DB::commit();
        }catch(Exception $e){
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => $e->getMessage()]);
        }


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan soal']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('question.update')) abort(404);

        $question = Question::find($request->id);
        $exam = $question->exam;
        $question_group = $question->questionGroup;

        return response()->json(['status' => 'success', 'data' => ['question' => $question, 'exam' => $exam, 'question_group' => $question_group]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('question.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $question_data = Question::find($request->id);

        $question_type = $request->question_type;
        $question = $request->question;
        $answer_1 = $request->answer_1;
        $point_1 = $request->point_1;
        $answer_2 = $request->answer_2;
        $point_2 = $request->point_2;
        $answer_3 = $request->answer_3;
        $point_3 = $request->point_3;
        $answer_4 = $request->answer_4;
        $point_4 = $request->point_4;
        $answer_5 = $request->answer_5;
        $point_5 = $request->point_5;

        $data = [
            'question_type' => $question_type,
            'question' => $question,
            'answer_1' => $answer_1,
            'point_1' => $point_1,
            'answer_2' => $answer_2,
            'point_2' => $point_2,
            'answer_3' => $answer_3,
            'point_3' => $point_3,
            'answer_4' => $answer_4,
            'point_4' => $point_4,
            'answer_5' => $answer_5,
            'point_5' => $point_5,
        ];

        $update = $question_data->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah soal']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('question.delete')) abort(404);

        $destroy = Question::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus soal']);
    }

    public function data(Exam $exam)
    {
        if($this->checkPermission('question.view')) abort(404);

        $questions = $exam->questions;
        if($exam->id == null) $questions = Question::all();

        return DataTables::of($questions)
                    ->editColumn('question_type', function($question) {
                        $question_type = "";

                        if($question->question_type == 'PG') $question_type = "<span class='badge badge-primary'>Pilihan Ganda</span>";
                        if($question->question_type == 'URAIAN') $question_type = "<span class='badge badge-warning'>Uraian</span>";

                        return $question_type;
                    })
                    ->addColumn('question_group', function($question) {
                        $question_group = "";

                        if($question->question_group_id == null){
                            $question_group = "-";
                        } else {
                            $question_group = $question->questionGroup->name;
                        }

                        return $question_group;
                    })
                    ->addColumn('action', function($question) {
                        $action = "";

                        if(auth()->user()->can('question.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Soal' data-id='{$question->id}' onclick='getUpdateQuestion(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('question.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Soal' data-id='{$question->id}' onclick='deleteQuestion(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
        ];

        return Validator::make($data, [
            'question_type' => ['required', 'string'],
            'question' => ['required', 'string'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
