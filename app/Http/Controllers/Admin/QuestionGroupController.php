<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\QuestionGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class QuestionGroupController extends Controller
{
    public function index()
    {
        if($this->checkPermission('question_group.view')) abort(404);

        return view('admin.question_group');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('question_group.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        DB::beginTransaction();
        try{
            $exam = Exam::find($request->exam_id);

            $insert = $exam->questionGroups()->create($data);

            DB::commit();
        }catch(Exception $e){
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => $e->getMessage()]);
        }


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan grup soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan grup soal']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('question_group.update')) abort(404);

        $question_group = QuestionGroup::find($request->id);
        $exam = $question_group->exam;

        return response()->json(['status' => 'success', 'data' => ['question_group' => $question_group, 'exam' => $exam]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('question_group.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $question_group = QuestionGroup::find($request->id);

        $name = $request->name;

        $data = [
            'name' => $name,
        ];

        $update = $question_group->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah grup soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah grup soal']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('question_group.delete')) abort(404);

        $destroy = QuestionGroup::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus grup soal']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus grup soal']);
    }

    public function data(Exam $exam)
    {
        if($this->checkPermission('question_group.view')) abort(404);

        $question_groups = $exam->questionGroups;
        if($exam->id == null) $question_groups = QuestionGroup::all();

        return DataTables::of($question_groups)
                    ->addColumn('exam_name', function($question_group) {
                        $exam_name = "";

                        $exam_name = $question_group->exam->name;

                        return $exam_name;
                    })
                    ->addColumn('action', function($question_group) {
                        $action = "";

                        if(auth()->user()->can('question_group.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Grup Soal' data-id='{$question_group->id}' onclick='getUpdateQuestionGroup(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('question_group.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Grup Soal' data-id='{$question_group->id}' onclick='deleteQuestionGroup(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function list(Exam $exam, Request $request)
    {
        $term = $request->data['term'];

        $search = $exam->questionGroups()->where('name', 'like', '%'.$term.'%')->get();
        if($exam->id == null) $search = QuestionGroup::where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
