<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Instructor;
use App\Models\Participant;
use App\Models\Training;
use App\Models\User;
use App\Models\Vocational;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class TrainingController extends Controller
{
    public function index()
    {
        if($this->checkPermission('training.view')) abort(404);

        return view('admin.training.index');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('training.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $year = Carbon::now()->year;
        $step = $request->step;
        $class = $request->class;
        $training_type = $request->training_type;

        $data = [
            'year' => $year,
            'step' => $step,
            'class' => $class,
            'training_type' => $training_type,
        ];

        DB::beginTransaction();

        try{

            $vocational_id = Vocational::find($request->vocational_id);

            if(!$vocational_id) return response()->json(['status' => 'error', 'title' => 'Sukses!', 'msg' => 'Kejuruan yang Anda pilih tidak tersedia']);

            $program_id = $vocational_id->programs()->find($request->program_id);

            if(!$program_id) return response()->json(['status' => 'error', 'title' => 'Sukses!', 'msg' => 'Program yang Anda pilih tidak tersedia']);


            $data['vocational_id'] = $request->vocational_id;
            $data['program_id'] = $request->program_id;

            $code = $this->genereateCode($request->vocational_id, $request->program_id, $year, $step, $class, $training_type);

            $data['code'] = $code;

            $insert = Training::create($data);

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'error', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan pelatihan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan pelatihan']);
    }

    public function storeParticipant(Training $training, Request $request)
    {
        if($this->checkPermission('training.manage')) abort(404);

        $user_id = $request->user_id;

        $insert = $training->addParticipant($user_id);

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan peserta']);
    }

    public function storeInstructor(Training $training, Request $request)
    {
        if($this->checkPermission('training.manage')) abort(404);

        $instructor_id = $request->instructor_id;

        $insert = $training->addInstructor($instructor_id);

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan peserta']);
    }

    public function storeCourse(Training $training, Request $request)
    {
        if($this->checkPermission('training.manage')) abort(404);

        $course_id = $request->course_id;

        $insert = $training->addCourse($course_id);

        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan peserta']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan peserta']);
    }

    public function show(Training $training)
    {
        if($this->checkPermission('training.view')) abort(404);

        return view('admin.training.show', ['training' => $training]);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('training.update')) abort(404);

        $training = Training::find($request->id);
        $vocational = $training->vocational;
        $program = $training->program;

        return response()->json(['status' => 'success', 'data' => ['training' => $training, 'vocational' => $vocational, 'program' => $program]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('training.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $training = Training::find($request->id);

        $vocational_id = $training->vocational_id;
        $program_id = $training->program_id;
        $year = $request->year;
        $step = $request->step;
        $class = $request->class;
        $training_type = $request->training_type;

        $data = [
            'year' => $year,
            'step' => $step,
            'class' => $class,
            'training_type' => $training_type,
        ];

        DB::beginTransaction();

        try{
            $code = $this->genereateCode($vocational_id, $program_id, $year, $step, $class, $training_type);

            $data['code'] = $code;

            $update = $training->update($data);

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'error', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah pelatihan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah pelatihan']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('training.delete')) abort(404);

        $destroy = Training::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus pelatihan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus pelatihan']);
    }

    public function data()
    {
        if($this->checkPermission('training.view')) abort(404);

        $trainings = Training::all();

        return DataTables::of($trainings)
                    ->addColumn('vocational_name', function($training) {
                        $vocational_name = "";

                        $vocational_name = $training->vocational->name;

                        return $vocational_name;
                    })
                    ->addColumn('program_name', function($training) {
                        $program_name = "";

                        $program_name = $training->program->name;

                        return $program_name;
                    })
                    ->editColumn('training_type', function($training) {
                        $training_type = "";

                        if($training->training_type == 'stay_overnight') $training_type = "<span class='badge badge-secondary'>Menginap</span>";
                        if($training->training_type == 'go_home') $training_type = "<span class='badge badge-primary'>Pulang</span>";

                        return $training_type;
                    })
                    ->editColumn('code', function($training) {
                        $code = "";

                        $code = "<a href='javascript:void(0)' tooltip='Klik untuk menyalin!' data-code='{$training->code}' onclick='copyCode(this);'><span class='badge badge-secondary'>{$training->code}</span></a>";

                        return $code;
                    })
                    ->addColumn('action', function($training) {
                        $action = "";

                        if(auth()->user()->can('training.manage')) $action .= "<a href='".route('admin.training.show', ['training' => $training])."' class='btn btn-icon btn-info' tooltip='Detail Pelatihan'><i class='far fa-eye'></i></a>&nbsp;";
                        if(auth()->user()->can('training.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Pelatihan' data-id='{$training->id}' onclick='getUpdateTraining(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('training.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Pelatihan' data-id='{$training->id}' onclick='deleteTraining(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function dataParticipant(Training $training)
    {
        if($this->checkPermission('training.view')) abort(404);

        $users = $training->users;
        if($training->id == null) abort(404);

        return DataTables::of($users)
                    ->addColumn('gender', function($user) {
                        $gender = "";

                        if($user->participant->gender == 'L') $gender = "<span class='badge badge-primary'>Laki - laki</span>";
                        if($user->participant->gender == 'P') $gender = "<span class='badge badge-warning'>Perempuan</span>";

                        return $gender;
                    })
                    ->addColumn('date_of_birth', function($user) {
                        $date_of_birth = "";

                        $date_of_birth = Carbon::parse($user->date_of_birth)->age . " tahun";

                        return $date_of_birth;
                    })
                    ->addColumn('education', function($user) {
                        $education = "";

                        $education = $user->participant->education;

                        return $education;
                    })
                    ->addColumn('job', function($user) {
                        $job = "";

                        $job = $user->participant->job;

                        return $job;
                    })
                    ->addColumn('action', function($user) {
                        $action = "";

                        if(auth()->user()->can('training.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Peserta' data-id='{$user->id}' onclick='deleteTrainingParticipant(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function dataInstructor(Training $training)
    {
        if($this->checkPermission('training.view')) abort(404);

        $instructors = $training->instructors;
        if($training->id == null) abort(404);

        return DataTables::of($instructors)
                    ->addColumn('action', function($instructor) {
                        $action = "";

                        if(auth()->user()->can('training.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Pengajar' data-id='{$instructor->id}' onclick='deleteTrainingInstructor(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function dataCourse(Training $training)
    {
        if($this->checkPermission('training.view')) abort(404);

        $courses = $training->courses;
        if($training->id == null) abort(404);

        return DataTables::of($courses)
                    ->addColumn('action', function($course) {
                        $action = "";

                        if(auth()->user()->can('training.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Mapel' data-id='{$course->id}' onclick='deleteTrainingCourse(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function listParticipant(Training $training, Request $request)
    {
        $id = array();
        $term = $request->data['term'];

        if($training->id == null) abort(404);

        $user_training = $training->users;

        foreach($user_training as $record){
                $id[] = $record->id;
        }

        $users = new User;

        foreach($users->all() as $record){
            if(!$record->hasRole('participant')){
                $id[] = $record->id;
            }
        }

        $search = $users->whereNotIn('id', $id)->where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    public function listInstructor(Training $training, Request $request)
    {
        $id = array();
        $term = $request->data['term'];

        if($training->id == null) abort(404);

        $instructor_training = $training->instructors;

        foreach($instructor_training as $record){
            $id[] = $record->id;
        }

        $search = Instructor::whereNotIn('id', $id)->where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    public function listCourse(Training $training, Request $request)
    {
        $id = array();
        $term = $request->data['term'];

        if($training->id == null) abort(404);

        $course_training = $training->courses;

        foreach($course_training as $record){
            $id[] = $record->id;
        }

        $search = Course::whereNotIn('id', $id)->where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    protected function genereateCode($vocational_id, $program_id, $year, $step, $class, $training_type)
    {
        $code = "";
        $training = "";

        if($training_type == 'stay_overnight') $training = "MN";
        if($training_type == 'go_home') $training = "PN";

        $code = $training.$year.$step.$class.'-'.$vocational_id.'-'.$program_id;

        return $code;
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
        ];

        return Validator::make($data, [
            'year' => ['required', 'numeric'],
            'step' => ['required', 'numeric'],
            'class' => ['required', 'numeric'],
            'training_type' => ['required', 'string'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
