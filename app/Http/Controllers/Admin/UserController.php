<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        if($this->checkPermission('user.view')) abort(404);

        $roles = Role::all();

        return view('admin.user', compact('roles'));
    }

    public function store(Request $request)
    {
        if($this->checkPermission('user.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ];

        DB::beginTransaction();

        if($request->role == 3){
            $validator = $this->validator($request->all(), 'insert', 'participant');

            if($validator->fails()){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
            }

            $data_participant = [
                'gender' => $request->gender,
                'date_of_birth' => $request->date_of_birth,
                'education' => $request->education,
                'job' => $request->job,
            ];
        }

        try{
            $insert = User::create($data);

            $role = Role::find($request->role);

            $insert->assignRole($role->name);

            if($request->role == 3){
                $insert->participant()->create($data_participant);
            }

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan pengguna']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan pengguna']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('user.update')) abort(404);

        $user = User::find($request->id);
        $role = $user->roles->first();
        $participant = [];
        if($role->id == 3){
            $participant = $user->participant;
        }

        return response()->json(['status' => 'success', 'data' => ['user' => $user, 'role' => $role, 'participant' => $participant]]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('user.update')) abort(404);

        $validator = $this->validator($request->all(), 'update');

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $user = User::find($request->id);

        $username = $user->username;
        $name = $request->name;
        $email = $user->email;
        $password = $user->password;

        if($username != $request->username){
            $check_username = User::where('username', '=', $request->username)->get()->count();

            if($check_username != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Nama pengguna yang Anda masukkan sudah terdaftar']);
            }

            $username = $request->username;
        }

        if($email != $request->email){
            $check_email = User::where('email', '=', $request->email)->get()->count();

            if($check_email != 0){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => 'Email yang Anda masukkan sudah terdaftar']);
            }

            $email = $request->email;
        }

        if($request->password != null){
            $password = bcrypt($request->password);
        }

        $data = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        DB::beginTransaction();

        if($request->role == 3){
            $validator = $this->validator($request->all(), 'update', 'participant');

            if($validator->fails()){
                return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
            }

            $data_participant = [
                'gender' => $request->gender,
                'date_of_birth' => $request->date_of_birth,
                'education' => $request->education,
                'job' => $request->job,
            ];
        }

        try{
            $update = $user->update($data);

            $role = Role::find($request->role);

            $user->syncRoles($role->name);

            if($request->role == 3){
                $user->participant()->update($data_participant);
            }

            DB::commit();
        }catch(Exception $e){
            DB::rollback();
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => $e->getMessage()]);
        }

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah pengguna']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah pengguna']);
    }

    public function getManage(Request $request)
    {
        if($this->checkPermission('user.manage')) abort(404);

        $permissions = Permission::all();
        $user = User::find($request->id);
        $view = "";

        $view .= '<input type="hidden" id="manage-user-id" name="id" value="'.$user->id.'" required>';

        $view .= '<div class="row">';
        $view .= '<div class="col-12 text-center">';
        $view .= '<h4>User '.ucfirst($user->name).'</h4>';
        $view .= '</div>';

        foreach($permissions as $permission){
            $checked = '';
            $fromRole = '';
            $setDisabled = '';

            foreach($permission->users as $user_permission){
                if($user->id == $user_permission->id){
                    $checked = 'checked';
                }
            }

            foreach($permission->roles as $role_permission){
                if($user->roles->first()->id == $role_permission->id){
                    $checked = 'checked';
                    $fromRole = "(Role Active)";
                    $setDisabled = "disabled";
                }
            }

            $view .= '<div class="form-group col-lg-6" style="margin:0!important;">';
            $view .= '<div class="custom-switches-stacked mt-2">';
            $view .= '<label class="custom-switch">';
            $view .= '<input type="checkbox" name="permission[]" value="'.$permission->id.'" class="custom-switch-input" '.$checked.' '.$setDisabled.'>';
            $view .= '<span class="custom-switch-indicator"></span>';
            $view .= '<span class="custom-switch-description">'.$permission->name.' '.$fromRole.'</span>';
            $view .= '</label>';
            $view .= '</div>';
            $view .= '</div>';
        }
        $view .= '</div>';

        return $view;
    }

    public function manage(Request $request)
    {
        if($this->checkPermission('user.manage')) abort(404);

        $permissions = $request->permission;
        $user = User::find($request->id);

        if(empty($permissions)){
            $user->permissions()->detach();

            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah izin pengguna '. ucfirst($user->name)]);
        }

        for($i = 0; $i < count($permissions); $i++) {
            $perms[] = Permission::find($permissions[$i]);
        }

        foreach($perms as $perm) {
            $data[] = $perm->name;
        }

        if(!empty($user)){
            $user->updatePermissions($data);
        }

        return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil mengubah izin pengguna '. ucfirst($user->name)]);;
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('user.delete')) abort(404);

        $user = User::find($request->id);

        if($user->profile_picture != null){
            Storage::delete($user->profile_picture);
        }

        $destroy = $user->delete();

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus pengguna']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus pengguna']);
    }

    public function data()
    {
        if($this->checkPermission('user.view')) abort(404);

        $users = User::all();

        return DataTables::of($users)
                    ->addColumn('action', function($user) {
                        $action = "";

                        if(auth()->user()->can('user.manage')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-success' tooltip='Mengelola Pengguna' data-id='{$user->id}' onclick='getManageUser(this);'><i class='fas fa-tasks'></i></a>&nbsp;";
                        if(auth()->user()->can('user.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Pengguna' data-id='{$user->id}' onclick='getUpdateUser(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('user.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Pengguna' data-id='{$user->id}' onclick='deleteUser(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    protected function validator(array $data, $type = 'insert', $role = 'developer')
    {
        $username = 'unique:users,username';
        $email = 'unique:users,email';
        $password = ['string', 'min:8', 'max:191', 'confirmed'];

        if($type == 'update'){
            $username = '';
            $email = '';
            $password = [];
        }

        $gender = [];
        $date_of_birth = [];
        $education = [];
        $job = [];

        if($role == 'participant'){
            $gender = ['required', 'string'];
            $date_of_birth = ['required', 'date'];
            $education = ['required', 'string'];
            $job = ['required', 'string'];
        }

        $message = [
            'required' => ':attribute tidak boleh kosong',
            'email' => ':attribute tidak valid',
            'string' => ':attribute harus bertipe String',
            'min' => ':attribute minimal :min karakter',
            'max' => ':attribute maksimal :max karakter',
            'apha_num' => ':attribute tidak boleh ada spasi',
            'confirmed' => ':atrribute yang dimasukkan tidak sama',
        ];

        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:191','alpha_num', $username],
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', $email],
            'password' => $password,
            'role' => ['required', 'string', 'max:191'],
            'gender' => $gender,
            'date_of_birth' => $date_of_birth,
            'education' => $education,
            'job' => $job,
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
