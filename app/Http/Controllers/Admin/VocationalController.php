<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vocational;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class VocationalController extends Controller
{
    public function index()
    {
        if($this->checkPermission('vocational.view')) abort(404);

        return view('admin.vocational');
    }

    public function store(Request $request)
    {
        if($this->checkPermission('vocational.create')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $data = [
            'name' => $request->name,
        ];

        $insert = Vocational::create($data);


        if($insert) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menambahkan kejuruan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menambahkan kejuruan']);
    }

    public function edit(Request $request)
    {
        if($this->checkPermission('vocational.update')) abort(404);

        $vocational = Vocational::find($request->id);

        return response()->json(['status' => 'success', 'data' => $vocational]);
    }

    public function update(Request $request)
    {
        if($this->checkPermission('vocational.update')) abort(404);

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning!', 'msg' => $validator->errors()->first()]);
        }

        $vocational = Vocational::find($request->id);

        $name = $request->name;

        $data = [
            'name' => $name,
        ];

        $update = $vocational->update($data);

        if($update) return response()->json(['status' => 'success', 'title' => 'Sukses', 'msg' => 'Berhasil mengubah kejuruan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal mengubah kejuruan']);
    }

    public function destroy(Request $request)
    {
        if($this->checkPermission('vocational.delete')) abort(404);

        $destroy = Vocational::destroy($request->id);

        if($destroy) return response()->json(['status' => 'success', 'title' => 'Sukses!', 'msg' => 'Berhasil menghapus kejuruan']);

        return response()->json(['status' => 'error', 'title' => 'Gagal!', 'msg' => 'Gagal menghapus kejuruan']);
    }

    public function data()
    {
        if($this->checkPermission('vocational.view')) abort(404);

        $vocationals = Vocational::all();

        return DataTables::of($vocationals)
                    ->addColumn('action', function($vocational) {
                        $action = "";

                        if(auth()->user()->can('vocational.update')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-primary' tooltip='Memperbarui Kejuruan' data-id='{$vocational->id}' onclick='getUpdateVocational(this);'><i class='far fa-edit'></i></a>&nbsp;";
                        if(auth()->user()->can('vocational.delete')) $action .= "<a href='javascript:void(0)' class='btn btn-icon btn-danger' tooltip='Menghapus Kejuruan' data-id='{$vocational->id}' onclick='deleteVocational(this);'><i class='fas fa-trash'></i></a>&nbsp;";

                        return $action;
                    })
                    ->escapeColumns([])
                    ->addIndexColumn()
                    ->make(true);
    }

    public function list(Request $request)
    {
        $term = $request->data['term'];

        $search = Vocational::where('name', 'like', '%'.$term.'%')->get();

        return response()->json($search);
    }

    protected function validator(array $data)
    {
        $message = [
            'required' => ':attribute tidak boleh kosong',
            'string' => ':attribute harus bertipe String',
            'max' => ':attribute maksimal :max karakter',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:191'],
        ], $message);
    }

    protected function checkPermission($permission)
    {
        return (bool) (!auth()->user()->can($permission));
    }
}
