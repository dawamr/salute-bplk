<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegisterMail;
use App\Models\Role;
use App\Models\Setting;
use App\Models\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute yang Anda masukkan sudah terdaftar',
            'email' => ':attribute yang Anda masukkan tidak valid',
            'max' => ':attribute tidak boleh lebih dari :max karakter',
            'min' => ':attribute minimal :min karakter',
            'date' => ':attribute harus berupa berformat Date',
            'password.confirmed' => ':attribute yang Anda masukkan tidak sama'
        ];
        return Validator::make($data, [
            'username' => ['required', 'string', 'min:6', 'max:255', 'unique:users,username'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'gender' => ['required', 'string'],
            'date_of_birth' => ['required', 'date'],
            'education' => ['required', 'string', 'max:255'],
            'job' => ['required', 'string', 'max:255'],
        ], $messages);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // $this->validator($request->all())->validate();
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return response()->json(['status' => 'warning', 'title' => 'Warning', 'msg' => $validator->errors()->first()]);
        };

        DB::beginTransaction();
        try{
            event(new Registered($user = $this->create($request->all())));

            // $this->guard()->login($user);

            $default = Role::first();
            $role = Role::where('default_user', 1)->first();

            $role = $role ? $role->name : $default->name;

            $user->assignRole($role);
            $createParticipant = $user->createParticipant($request->gender, $request->date_of_birth, $request->education, $request->job);

            // $from = Setting::where('name', '=', 'email_verification')->first();
            // $url_confirmation = url('/confirmation/'.$user->token);

            // Mail::to($request->email)->send(new RegisterMail(['from' => $from->value], $url_confirmation));

            DB::commit();
        }catch(Exception $e){
            return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => $e->getMessage()]);
        }

        if($createParticipant){
            return response()->json(['status' => 'success', 'title' => 'Berhasil Daftar', 'msg' => 'Silakan aktivasi email Anda terlebih dahulu']);
        }

        return response()->json(['status' => 'error', 'title' => 'Gagal', 'msg' => 'Gagal melakukan pendaftaran']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'activated' => 0,
            'token' => Str::random(150),
        ]);
    }
}
