<?php

namespace App\Http\ViewComposers;

use App\Models\Setting;
use Illuminate\View\View;

class SettingComposer
{
    public function compose(View $view)
    {
        $settings = Setting::get();

        $app_logo = $settings->where('name', 'app_logo')->first()->value;

        if($settings->where('name', 'app_logo')->first()->value == null) {
            $app_logo = 'uploads/site/logo/default.png';
        }
        $view->with([
            'app_name' => $settings->where('name', 'app_name')->first()->value,
            'app_description' => $settings->where('name', 'app_description')->first()->value,
            'app_author' => $settings->where('name', 'app_author')->first()->value,
            'app_version' => $settings->where('name', 'app_version')->first()->value,
            'app_logo' => $app_logo,
            'meta_author' => $settings->where('name', 'meta_author')->first()->value,
            'meta_description' => $settings->where('name', 'meta_description')->first()->value,
            'app_name_small' => substr($settings->where('name', 'app_name')->first()->value, 0, 2),
            'email_verification' => $settings->where('name', 'email_verification')->first()->value,
            'email_information' => $settings->where('name', 'email_information')->first()->value,
        ]);
    }
}
