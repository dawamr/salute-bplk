<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    public $from;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $url)
    {
        $this->from = $from;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->from)
                    ->view('mail.register')
                    ->with([
                        'url_confirmation' => $this->url,
                    ]);
    }
}
