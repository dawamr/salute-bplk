<?php

namespace App\Models;

use App\Traits\AnswerTrait;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use AnswerTrait;

    protected $fillable = ['answer', 'point'];
}
