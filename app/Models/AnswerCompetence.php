<?php

namespace App\Models;

use App\Traits\AnswerCompetenceTrait;
use Illuminate\Database\Eloquent\Model;

class AnswerCompetence extends Model
{
    use AnswerCompetenceTrait;

    protected $fillable = ['competence', 'test', 'test_result'];
}
