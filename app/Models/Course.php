<?php

namespace App\Models;

use App\Traits\CourseTrait;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use CourseTrait;

    protected $fillable = ['name', 'description', 'stay_overnight', 'go_home'];
}
