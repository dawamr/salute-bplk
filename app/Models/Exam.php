<?php

namespace App\Models;

use App\Traits\ExamTrait;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use ExamTrait;

    protected $fillable = ['name'];
}
