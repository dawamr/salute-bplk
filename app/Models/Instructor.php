<?php

namespace App\Models;

use App\Traits\InstructorTrait;
use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    use InstructorTrait;

    protected $fillable = ['name'];
}
