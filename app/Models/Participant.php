<?php

namespace App\Models;

use App\Traits\ParticipantTrait;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use ParticipantTrait;

    protected $fillable = ['gender', 'date_of_birth', 'education', 'job'];
}
