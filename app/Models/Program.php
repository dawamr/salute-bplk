<?php

namespace App\Models;

use App\Traits\ProgramTrait;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use ProgramTrait;

    protected $fillable = ['name'];
}
