<?php

namespace App\Models;

use App\Traits\QuestionTrait;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use QuestionTrait;

    protected $fillable = ['question_group_id', 'question_type', 'question', 'answer_1', 'answer_2', 'answer_3', 'answer_4', 'answer_5', 'point_1', 'point_2', 'point_3', 'point_4', 'point_5',];
}
