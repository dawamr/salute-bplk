<?php

namespace App\Models;

use App\Traits\QuestionGroupTrait;
use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    use QuestionGroupTrait;

    protected $fillable = ['name'];
}
