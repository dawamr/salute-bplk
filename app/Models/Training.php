<?php

namespace App\Models;

use App\Traits\TrainingTrait;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use TrainingTrait;

    protected $fillable = ['vocational_id', 'program_id', 'year', 'step', 'class', 'training_type', 'code'];

    public function getRouteKeyName()
    {
        return 'code';
    }
}
