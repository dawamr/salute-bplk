<?php

namespace App\Models;

use App\Traits\VocationalTrait;
use Illuminate\Database\Eloquent\Model;

class Vocational extends Model
{
    use VocationalTrait;

    protected $fillable = ['name'];
}
