<?php

namespace App\Traits;

use App\Models\Answer;

trait AnswerCompetenceTrait
{
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
