<?php

namespace App\Traits;

use App\Models\AnswerCompetence;
use App\Models\Question;
use App\Models\Training;
use App\Models\User;

trait AnswerTrait
{
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function trainings()
    {
        return $this->belongsTo(Training::class);
    }

    public function questions()
    {
        return $this->belongsTo(Question::class);
    }

    public function answerCompetences()
    {
        return $this->hasMany(AnswerCompetence::class);
    }
}
