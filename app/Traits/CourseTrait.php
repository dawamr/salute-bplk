<?php

namespace App\Traits;

use App\Models\Exam;
use App\Models\Training;

trait CourseTrait
{
    public function trainings()
    {
        return $this->belongsToMany(Training::class, 'training_course');
    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
}
