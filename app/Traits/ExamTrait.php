<?php

namespace App\Traits;

use App\Models\Course;
use App\Models\Question;
use App\Models\QuestionGroup;
use App\Models\User;

trait ExamTrait
{
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function questionGroups()
    {
        return $this->hasMany(QuestionGroup::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
