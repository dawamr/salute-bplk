<?php

namespace App\Traits;

use App\Models\Training;
use App\Models\User;

trait InstructorTrait
{
    public function trainings()
    {
        return $this->belongsToMany(Training::class, 'training_instructor');
    }
}
