<?php

namespace App\Traits;

use App\Models\Training;
use App\Models\Vocational;

trait ProgramTrait
{
    public function vocational()
    {
        return $this->belongsTo(Vocational::class);
    }

    public function trainings()
    {
        return $this->hasMany(Training::class);
    }
}
