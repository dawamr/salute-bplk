<?php

namespace App\Traits;

use App\Models\Exam;
use App\Models\Question;

trait QuestionGroupTrait
{
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    public function question()
    {
        return $this->hasMany(Question::class);
    }
}
