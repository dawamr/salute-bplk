<?php

namespace App\Traits;

use App\Models\Answer;
use App\Models\Exam;
use App\Models\QuestionGroup;

trait QuestionTrait
{
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    public function questionGroup()
    {
        return $this->belongsTo(QuestionGroup::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
