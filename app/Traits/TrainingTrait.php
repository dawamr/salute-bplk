<?php

namespace App\Traits;

use App\Models\Answer;
use App\Models\Course;
use App\Models\Instructor;
use App\Models\Program;
use App\Models\User;
use App\Models\Vocational;
use Illuminate\Support\Arr;

trait TrainingTrait
{
    public function addParticipant(...$id)
    {
        $id = $this->getAllUser(Arr::flatten($id));

        if($id == null) {
            return $this;
        }

        $this->users()->saveMany($id);

        return $this;

    }

    public function destroyParticipant(... $id)
    {
        $id = $this->getAllUser($id);

        $this->users()->detach($id);

        return $this;
    }

    public function syncParticipant(...$id)
    {
        $this->users()->detach();

        return $this->addParticipant($id);
    }

    protected function getAllUser(array $id)
    {
        return User::whereIn('id', $id)->get();
    }

    public function addInstructor(...$id)
    {
        $id = $this->getAllInstructor(Arr::flatten($id));

        if($id == null) {
            return $this;
        }

        $this->instructors()->saveMany($id);

        return $this;

    }

    public function destroyInstructor(... $id)
    {
        $id = $this->getAllInstructor($id);

        $this->instructors()->detach($id);

        return $this;
    }

    public function syncInstructor(...$id)
    {
        $this->instructors()->detach();

        return $this->addInstructor($id);
    }

    protected function getAllInstructor(array $id)
    {
        return Instructor::whereIn('id', $id)->get();
    }

    public function addCourse(...$id)
    {
        $id = $this->getAllCourse(Arr::flatten($id));

        if($id == null) {
            return $this;
        }

        $this->courses()->saveMany($id);

        return $this;

    }

    public function destroyCourse(... $id)
    {
        $id = $this->getAllCourse($id);

        $this->courses()->detach($id);

        return $this;
    }

    public function syncCourse(...$id)
    {
        $this->courses()->detach();

        return $this->addCourse($id);
    }

    protected function getAllCourse(array $id)
    {
        return Course::whereIn('id', $id)->get();
    }

    public function instructors()
    {
        return $this->belongsToMany(Instructor::class, 'training_instructor');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'training_course');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'training_user')->withTimestamps();
    }

    public function vocational()
    {
        return $this->belongsTo(Vocational::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
