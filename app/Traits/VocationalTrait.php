<?php

namespace App\Traits;

use App\Models\Program;
use App\Models\Training;

trait VocationalTrait
{
    public function programs()
    {
        return $this->hasMany(Program::class);
    }

    public function trainings()
    {
        return $this->hasMany(Training::class);
    }
}
