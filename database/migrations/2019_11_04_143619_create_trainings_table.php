<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vocational_id')->unsigned();
            $table->bigInteger('program_id')->unsigned();
            $table->string('year');
            $table->string('step');
            $table->string('class');
            $table->enum('training_type', ['stay_overnight', 'go_home']);
            $table->string('code')->unique();
            $table->timestamps();

            $table->foreign('vocational_id')->references('id')->on('vocationals')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('program_id')->references('id')->on('programs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
