<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingInstructorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_instructor', function (Blueprint $table) {
            $table->bigInteger('training_id')->unsigned();
            $table->bigInteger('instructor_id')->unsigned();

            $table->foreign('training_id')->references('id')->on('trainings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('instructor_id')->references('id')->on('instructors')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['training_id', 'instructor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_instructor');
    }
}
