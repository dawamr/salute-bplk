<?php

use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'setting.create', 'guard_name' => 'Menambahkan Pengaturan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'setting.view', 'guard_name' => 'Melihat Pengaturan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'setting.update', 'guard_name' => 'Memperbarui Pengaturan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'setting.delete', 'guard_name' => 'Menghapus Pengaturan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'role.create', 'guard_name' => 'Menambahkan Peran', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'role.view', 'guard_name' => 'Melihat Peran', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'role.update', 'guard_name' => 'Memperbarui Peran', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'role.delete', 'guard_name' => 'Menghapus Peran', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'role.manage', 'guard_name' => 'Mengelola Peran', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'permission.create', 'guard_name' => 'Menambahkan Izin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'permission.view', 'guard_name' => 'Melihat Izin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'permission.update', 'guard_name' => 'Memperbarui Izin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'permission.delete', 'guard_name' => 'Menghapus Izin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'user.create', 'guard_name' => 'Menambahkan Pengguna', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'user.view', 'guard_name' => 'Melihat Pengguna', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'user.update', 'guard_name' => 'Memperbarui Pengguna', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'user.delete', 'guard_name' => 'Menghapus Pengguna', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'user.manage', 'guard_name' => 'Mengelola Pengguna', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'vocational.create', 'guard_name' => 'Menambahkan Kejuruan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'vocational.view', 'guard_name' => 'Melihat Kejuruan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'vocational.update', 'guard_name' => 'Memperbarui Kejuruan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'vocational.delete', 'guard_name' => 'Menghapus Kejuruan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'program.create', 'guard_name' => 'Menambahkan Program', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'program.view', 'guard_name' => 'Melihat Program', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'program.update', 'guard_name' => 'Memperbarui Program', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'program.delete', 'guard_name' => 'Menghapus Program', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'instructor.create', 'guard_name' => 'Menambahkan Pengajar', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'instructor.view', 'guard_name' => 'Melihat Pengajar', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'instructor.update', 'guard_name' => 'Memperbarui Pengajar', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'instructor.delete', 'guard_name' => 'Menghapus Pengajar', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'participant.create', 'guard_name' => 'Menambahkan Peserta', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'participant.view', 'guard_name' => 'Melihat Peserta', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'participant.update', 'guard_name' => 'Memperbarui Peserta', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'participant.delete', 'guard_name' => 'Menghapus Peserta', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'course.create', 'guard_name' => 'Menambahkan Mapel', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'course.view', 'guard_name' => 'Melihat Mapel', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'course.update', 'guard_name' => 'Memperbarui Mapel', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'course.delete', 'guard_name' => 'Menghapus Mapel', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'exam.create', 'guard_name' => 'Menambahkan Materi', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'exam.view', 'guard_name' => 'Melihat Materi', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'exam.update', 'guard_name' => 'Memperbarui Materi', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'exam.delete', 'guard_name' => 'Menghapus Materi', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'question_group.create', 'guard_name' => 'Menambahkan Grup Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question_group.view', 'guard_name' => 'Melihat Grup Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question_group.update', 'guard_name' => 'Memperbarui Grup Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question_group.delete', 'guard_name' => 'Menghapus Grup Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'question.create', 'guard_name' => 'Menambahkan Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question.view', 'guard_name' => 'Melihat Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question.update', 'guard_name' => 'Memperbarui Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'question.delete', 'guard_name' => 'Menghapus Soal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'training.create', 'guard_name' => 'Menambahkan Pelatihan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'training.view', 'guard_name' => 'Melihat Pelatihan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'training.update', 'guard_name' => 'Memperbarui Pelatihan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'training.delete', 'guard_name' => 'Menghapus Pelatihan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'training.manage', 'guard_name' => 'Mengelola Pelatihan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],

            ['name' => 'report.view', 'guard_name' => 'Melihat Laporan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'report.manage', 'guard_name' => 'Mengelola Laporan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        foreach($permissions as $permission){
            $insert = Permission::create($permission);
            $insert->assignRole('developer');
        }
    }
}
