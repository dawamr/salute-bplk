<?php

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['name' => 'app_name', 'value' => 'Salute BPLK', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'app_description', 'value' => 'Sistem Evaluasi Online yang memudahkan dalam mendata semua inputan masuk', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'app_author', 'value' => 'Rio Prastiawan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'app_version', 'value' => '0.0.1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'app_logo', 'value' => null, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'meta_author', 'value' => 'Rio Prastiawan', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'meta_description', 'value' => 'Sistem Evaluasi Online yang memudahkan dalam mendata semua inputan masuk', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'email_verification', 'value' => 'akunviprio@gmail.com', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'email_information', 'value' => 'akunviprio@gmail.com', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        Setting::insert($settings);
    }
}
