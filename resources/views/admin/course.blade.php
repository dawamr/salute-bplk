@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Mapel</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Mapel</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Mapel</h4>
                        @can ('course.create')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-course').modal('show');setTimeout(() => {$('#add-course-name').focus()},500)" tooltip="Tambah Mapel"><i class="fas fa-plus"></i> Tambah Mapel</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="course-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can ('course.create')
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-course">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Mapel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-course">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="add-course-name" required autofocus>
                                </div>
                                <div class="form-group col-12">
                                    <label>Deskripsi</label>
                                    <textarea class="form-control" name="description" id="add-course-description" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-course">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('course.update')
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-update-course">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Mapel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-course">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-course-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="update-course-name" required autofocus>
                                </div>
                                <div class="form-group col-12">
                                    <label>Deskripsi</label>
                                    <textarea class="form-control" name="description" id="update-course-description" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-course">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getCourses();

            @can ('course.create')
                $("#form-add-course").on("submit", function(e) {
                    e.preventDefault();
                    addCourse();
                });
            @endcan

            @can ('course.update')
                $("#form-update-course").on("submit", function(e) {
                    e.preventDefault();
                    updateCourse();
                });
            @endcan
        });

        async function getCourses()
        {
            $("#course-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.course.data') }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'name' },
                    { data: 'description' },
                    { data: 'action' },
                ]
            });
        }

        @can ('course.create')
            async function addCourse()
            {
                var formData = $("#form-add-course").serialize();

                $.ajax({
                    url: "{{ route('admin.course.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-course").addClass('btn-progress');
                        $("#btn-add-course").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("textarea").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-course").removeClass('btn-progress');
                        $("#btn-add-course").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("textarea").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-course")[0].reset();
                            $('#modal-add-course').modal('hide');
                            getCourses();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('course.update')
            async function getUpdateCourse(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-course').modal('show');
                $('#form-update-course')[0].reset();

                $.ajax({
                    url: "{{ route('admin.course.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-course").addClass('btn-progress');
                        $("#btn-update-course").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("textarea").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-course").removeClass('btn-progress');
                        $("#btn-update-course").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("textarea").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-course-id').val(result['data']['id']);
                        $('#update-course-name').val(result['data']['name']);
                        $('#update-course-description').val(result['data']['description']);
                        setTimeout(() => {
                            $('#update-course-name').focus()
                        }, 1);
                    }
                });
            }

            async function updateCourse()
            {
                var formData = $("#form-update-course").serialize();

                $.ajax({
                    url: "{{ route('admin.course.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-course").addClass('btn-progress');
                        $("#btn-update-course").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("textarea").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-course").removeClass('btn-progress');
                        $("#btn-update-course").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("textarea").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-course")[0].reset();
                            $('#modal-update-course').modal('hide');
                            getCourses();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('course.delete')
            async function deleteCourse(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus mapel?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.course.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    getCourses();
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
