@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Materi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Materi</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h4>Filter</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group mb-0">
                            <select name="filter" id="filter" class="form-control select2-filter" onchange="getExams(this.value)"></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Materi</h4>
                        @can ('exam.create')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-exam').modal('show');setTimeout(() => {$('#add-exam-name').focus()},500)" tooltip="Tambah Materi"><i class="fas fa-plus"></i> Tambah Materi</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="exam-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Mapel</th>
                                        <th>Nama</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can ('exam.create')
        <div class="modal fade" role="dialog" id="modal-add-exam">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Materi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-exam">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <select name="course_id" id="add-exam-course-id" class="form-control select2-form"></select>
                                </div>
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="add-exam-name" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-exam">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('exam.update')
        <div class="modal fade" role="dialog" id="modal-update-exam">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Materi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-exam">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-exam-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <input type="text" class="form-control" name="course_id" id="update-exam-course-id" readonly>
                                </div>
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="update-exam-name" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-exam">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getExams();

            initSelect2Filter();
            initSelect2Form();

            @can ('exam.create')
                $("#form-add-exam").on("submit", function(e) {
                    e.preventDefault();
                    addExam();
                });
            @endcan

            @can ('exam.update')
                $("#form-update-exam").on("submit", function(e) {
                    e.preventDefault();
                    updateExam();
                });
            @endcan
        });

        async function getExams(filter = '')
        {
            $("#exam-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.exam.data') }}/"+filter,
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'course_name' },
                    { data: 'name' },
                    { data: 'action' },
                ]
            });
        }

        async function initSelect2Filter()
        {
            $('.select2-filter').select2({
                width: '100%',
                placeholder: "Filter",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.course.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        async function initSelect2Form()
        {
            $('.select2-form').select2({
                width: '100%',
                placeholder: "Pilih Mapel",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.course.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        @can ('exam.create')
            async function addExam()
            {
                var formData = $("#form-add-exam").serialize();

                $.ajax({
                    url: "{{ route('admin.exam.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-exam").addClass('btn-progress');
                        $("#btn-add-exam").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-exam").removeClass('btn-progress');
                        $("#btn-add-exam").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-exam")[0].reset();
                            $('#modal-add-exam').modal('hide');
                            getExams();
                            $('#filter').removeAttr('selected');
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('exam.update')
            async function getUpdateExam(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-exam').modal('show');
                $('#form-update-exam')[0].reset();

                $.ajax({
                    url: "{{ route('admin.exam.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-exam").addClass('btn-progress');
                        $("#btn-update-exam").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-exam").removeClass('btn-progress');
                        $("#btn-update-exam").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-exam-id').val(result['data']['exam']['id']);
                        $('#update-exam-name').val(result['data']['exam']['name']);
                        $('#update-exam-course-id').val(result['data']['course']['name']);
                        setTimeout(() => {
                            $('#update-exam-name').focus()
                        }, 1);
                    }
                });
            }

            async function updateExam()
            {
                var formData = $("#form-update-exam").serialize();

                $.ajax({
                    url: "{{ route('admin.exam.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-exam").addClass('btn-progress');
                        $("#btn-update-exam").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-exam").removeClass('btn-progress');
                        $("#btn-update-exam").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-exam")[0].reset();
                            $('#modal-update-exam').modal('hide');
                            getExams();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('exam.delete')
            async function deleteExam(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus exam?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.exam.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    getExams();
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
