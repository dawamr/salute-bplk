@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Peserta</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Peserta</div>
        </div>
    </div>

    <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Daftar Peserta</h4>
                            @can ('participant.create')
                                <div class="card-header-action">
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-participant').modal('show');setTimeout(() => {$('#add-participant-username').focus()},500)" tooltip="Tambah Peserta"><i class="fas fa-plus"></i> Tambah Peserta</a>
                                </div>
                            @endcan
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="participant-list">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="10">
                                                #
                                            </th>
                                            <th>Nama</th>
                                            <th>Nama Pengguna</th>
                                            <th>Email</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Pendidikan</th>
                                            <th>Pekerjaan</th>
                                            <th width="150">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('modals')
    @can ('participant.create')
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-participant">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Peserta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-participant">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" id="add-participant-username" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="add-participant-name" required>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" id="add-participant-email" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" id="add-participant-password" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="add-participant-password-confirm" required>
                                </div>
                                <div class="divider"></div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" id="add-participant-gender-l" value="L" class="selectgroup-input" checked="" required>
                                            <span class="selectgroup-button">Laki - laki</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" id="add-participant-gender-p" value="P" class="selectgroup-input" required>
                                            <span class="selectgroup-button">Perempuan</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="add-participant-date-of-birth" class="form-control datepicker" value="" name="date_of_birth" required autocomplete="off">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Pendidikan</label>
                                    <select name="education" id="add-participant-education" class="form-control selectric" required autocomplete="off">
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA">SMA</option>
                                        <option value="DIPLOMA">DIPLOMA</option>
                                        <option value="S1">S1</option>
                                        <option value="S2">S2</option>
                                        <option value="S3">S3</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Pekerjaan</label>
                                    <input type="text" id="add-participant-job" class="form-control" name="job" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-participant">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('participant.update')
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-update-participant">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Peserta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-participant">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-participant-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" id="update-participant-username" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="update-participant-name" required>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" id="update-participant-email" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" id="update-participant-password">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="update-participant-password-confirm">
                                </div>
                                <div class="divider"></div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" id="update-participant-gender-l" value="L" class="selectgroup-input" checked="" required>
                                            <span class="selectgroup-button">Laki - laki</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="gender" id="update-participant-gender-p" value="P" class="selectgroup-input" required>
                                            <span class="selectgroup-button">Perempuan</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="update-participant-date-of-birth" class="form-control datepicker" value="" name="date_of_birth" required autocomplete="off">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Pendidikan</label>
                                    <select name="education" id="update-participant-education" class="form-control selectric" required autocomplete="off">
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA">SMA</option>
                                        <option value="DIPLOMA">DIPLOMA</option>
                                        <option value="S1">S1</option>
                                        <option value="S2">S2</option>
                                        <option value="S3">S3</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Pekerjaan</label>
                                    <input type="text" id="update-participant-job" class="form-control" name="job" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-participant">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getParticipants();

            @can ('participant.create')
                $("#form-add-participant").on("submit", function(e) {
                    e.preventDefault();
                    addParticipant();
                });
            @endcan

            @can ('participant.update')
                $("#form-update-participant").on("submit", function(e) {
                    e.preventDefault();
                    updateParticipant();
                });
            @endcan
        });

        async function getParticipants()
        {
            $("#participant-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.participant.data') }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'name' },
                    { data: 'username' },
                    { data: 'email' },
                    { data: 'gender' },
                    { data: 'education' },
                    { data: 'job' },
                    { data: 'action' },
                ]
            });
        }

        @can ('participant.create')
            async function addParticipant()
            {
                var formData = $("#form-add-participant").serialize();

                $.ajax({
                    url: "{{ route('admin.participant.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-participant").addClass('btn-progress');
                        $("#btn-add-participant").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-participant").removeClass('btn-progress');
                        $("#btn-add-participant").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-participant")[0].reset();
                            $('#modal-add-participant').modal('hide');
                            getParticipants();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('participant.update')
            async function getUpdateParticipant(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-participant').modal('show');
                $('#form-update-participant')[0].reset();

                $.ajax({
                    url: "{{ route('admin.participant.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-participant").addClass('btn-progress');
                        $("#btn-update-participant").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-participant").removeClass('btn-progress');
                        $("#btn-update-participant").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-participant-id').val(result['data']['user']['id']);
                        $('#update-participant-username').val(result['data']['user']['username']);
                        $('#update-participant-name').val(result['data']['user']['name']);
                        $('#update-participant-email').val(result['data']['user']['email']);

                        if(result['data']['participant']['gender'] == 'L'){
                            $('#update-participant-gender-l').prop('checked', true);
                        }
                        if(result['data']['participant']['gender'] == 'P'){
                            $('#update-participant-gender-p').prop('checked', true);
                        }
                        $('#update-participant-date-of-birth').val(result['data']['participant']['date_of_birth']);
                        $('#update-participant-education').val(result['data']['participant']['education']);
                        $('#update-participant-job').val(result['data']['participant']['job']);

                        setTimeout(() => {
                            $('#update-participant-name').focus()
                        }, 1);
                    }
                });
            }

            async function updateParticipant()
            {
                var formData = $("#form-update-participant").serialize();

                $.ajax({
                    url: "{{ route('admin.participant.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-participant").addClass('btn-progress');
                        $("#btn-update-participant").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-participant").removeClass('btn-progress');
                        $("#btn-update-participant").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-participant")[0].reset();
                            $('#modal-update-participant').modal('hide');
                            getParticipants();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('participant.delete')
            async function deleteParticipant(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus peserta?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.participant.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    getParticipants();
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
