@extends('templates.admin')

@section('content')
<div class="section-header">
    <h1>Profil</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
        <div class="breadcrumb-item">Profil</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">Hai, {{ auth()->user()->name }}!</h2>
    <p class="section-lead">
        Ubah informasi tentang diri Anda di halaman ini.
    </p>

    <div class="row mt-sm-4">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <form method="post" class="needs-validation" novalidate="" action="javascript:void(0)" id="form-update-profile" enctype="multipart/form-data">
                    <div class="card-header">
                        <h4>Perbarui Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @csrf
                            @method('PUT')
                            <div class="form-group col-md-6 col-12">
                                <label>Nama Pengguna</label>
                                <input type="text" class="form-control" name="username" value="{{ auth()->user()->username }}" required="" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" value="{{ auth()->user()->name }}" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-12">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>Kata Sandi</label>
                                <input type="password" class="form-control" name="password" placeholder="Kosongkan password jika tidak ingin diubah" autocomplete="off">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Konfirmasi Kata Sandi</label>
                                <input type="password" class="form-control" name="password_confirmation" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-12">
                                <label>Foto Profil</label>
                                <div id="update-profile-picture-preview" class="image-preview">
                                    <label for="update-profile-picture-upload" id="update-profile-picture-label">Pilih Gambar</label>
                                    <input type="file" name="profile_picture" id="update-profile-picture-upload" />
                                </div>
                                @if (auth()->user()->profile_picture != null)
                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="checkbox" class="custom-control-input" id="null-profile-picture" name="null_profile_picture" value="1">
                                    <label class="custom-control-label" for="null-profile-picture">Kosongkan Foto Profile</label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-primary" type="submit" id="btn-update-profile">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
@endsection

@section('js-script')
<script>

    $(function() {
        "use strict";

        imagePreview();
        $('#update-profile-picture-preview').css('background', `url('{{ auth()->user()->profile_picture == null ? asset('uploads/user/profile/default.png') : asset(auth()->user()->profile_picture) }}') center center / cover transparent`);

        $("#form-update-profile").on("submit", function(e) {
            e.preventDefault();

            var formData = new FormData(this);
            updateProfile(formData);
        });
    });

    async function imagePreview()
    {
        $.uploadPreview({
            input_field: "#update-profile-picture-upload",
            preview_box: "#update-profile-picture-preview",
            label_field: "#update-profile-picture-label",
            label_default: "Choose File",
            label_selected: "Change File",
            no_label: false,
            success_callback: null
        });
    }

    async function updateProfile(formData)
    {

        $.ajax({
            url: "{{ route('admin.profile.update') }}",
            type: "POST",
            dataType: "json",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend() {
                $("input").attr('disabled', 'disabled');
                $("#btn-update-profile").addClass('btn-progress');

            },
            complete() {
                $("input").removeAttr('disabled', 'disabled');
                $("#btn-update-profile").removeClass('btn-progress');
            },
            success : function(result) {
                $('input[type="password"]').val('');

                notification(result['status'], result['title'], result['msg']);
            }
        });
    }
</script>
@endsection
