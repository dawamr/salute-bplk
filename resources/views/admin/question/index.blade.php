@extends('templates.admin')

@section('css-page')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/course-question-loading.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Soal</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Soal</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row" id="show-course-question">

        </div>
    </div>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getCourse();
        });

        async function getCourse()
        {
            $.ajax({
                url: "{{ route('admin.course.data.question') }}",
                type: "GET",
                dataType: "html",
                beforeSend() {
                    setLoading();
                },
                complete() {
                },
                success : function(result) {
                    $('#show-course-question').html(result);
                }
            });
        }
        async function setLoading()
        {
            var html = "";
            for(var i=0; i <= 6; i++){
                html += `<div class="col-12 col-lg-4 col-md-4 col-sm-12"><div class="card course-question"><div class="card-body course-question-info"><div class="course-question-place course-question-title"></div><div class="course-question-place course-question-title"></div><div class="course-question-place course-question-title"></div><div class="course-question-place course-question-title"></div></div></div></div>`;
            }
            $('#show-course-question').html(html);
        }

        async function redirectDetail(object)
        {
            var link = $(object).data('link');

            window.location=link;
        }
    </script>
@endsection
