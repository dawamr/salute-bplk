@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('admin.question') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Soal</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">{{ $course->name }}</div>
            <div class="breadcrumb-item">Soal</div>
        </div>
    </div>

    <div class="section-body">
        @foreach ($course->exams as $exam)
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $exam->name }}</h4>
                            @can ('question.create')
                                <div class="card-header-action">
                                    <a href="javascript:void(0)" class="btn btn-primary" data-id="{{ $exam->id }}" data-name="{{ $exam->name }}" onclick="openModalAdd(this);" tooltip="Tambah Soal"><i class="fas fa-plus"></i> Tambah Soal</a>
                                </div>
                            @endcan
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="question-{{ $exam->id }}">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="10">
                                                #
                                            </th>
                                            <th>Soal</th>
                                            <th>Tipe</th>
                                            <th>Grup</th>
                                            <th width="150">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('modals')
    @can ('question.create')
        <div class="modal fade" role="dialog" id="modal-add-question">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Soal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-question">
                        @csrf
                        <input type="hidden" name="exam_id" id="add-question-exam-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <input type="text" class="form-control" id="add-question-exam-id-view" readonly>
                                </div>
                                <div class="form-group col-12">
                                    <label>Grup Soal</label>
                                    <select name="question_group_id" id="add-question-question-grup-id" class="form-control select2-form"></select>
                                </div>
                                <div class="form-group col-12">
                                    <label>Tipe Soal</label>
                                    <select name="question_type" id="add-question-question-type" class="form-control selectric" required>
                                        <option value="PG">Pilihan Ganda</option>
                                        <option value="URAIAN">Uraian</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label>Soal</label>
                                    <input type="text" class="form-control" name="question" id="add-question-question" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 1</label>
                                    <input type="text" class="form-control" name="answer_1" id="add-question-answer-1">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 1</label>
                                    <input type="text" class="form-control" name="point_1" id="add-question-point-1">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 2</label>
                                    <input type="text" class="form-control" name="answer_2" id="add-question-answer-2">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 2</label>
                                    <input type="text" class="form-control" name="point_2" id="add-question-point-2">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 3</label>
                                    <input type="text" class="form-control" name="answer_3" id="add-question-answer-3">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 3</label>
                                    <input type="text" class="form-control" name="point_3" id="add-question-point-3">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 4</label>
                                    <input type="text" class="form-control" name="answer_4" id="add-question-answer-4">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 4</label>
                                    <input type="text" class="form-control" name="point_4" id="add-question-point-4">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 5</label>
                                    <input type="text" class="form-control" name="answer_5" id="add-question-answer-5">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 5</label>
                                    <input type="text" class="form-control" name="point_5" id="add-question-point-5">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-question">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('question.update')
        <div class="modal fade" role="dialog" id="modal-update-question">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Soal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-question">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-question-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <input type="text" class="form-control" name="exam" id="update-question-exam-id" readonly>
                                </div>
                                <div class="form-group col-12">
                                    <label>Grup Soal</label>
                                    <input type="text" class="form-control" name="question_group_id" id="update-question-question-group-id" readonly>
                                </div>
                                <div class="form-group col-12">
                                    <label>Tipe Soal</label>
                                    <select name="question_type" id="update-question-question-type" class="form-control selectric" required>
                                        <option value="PG">Pilihan Ganda</option>
                                        <option value="URAIAN">Uraian</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label>Soal</label>
                                    <input type="text" class="form-control" name="question" id="update-question-question" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 1</label>
                                    <input type="text" class="form-control" name="answer_1" id="update-question-answer-1">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 1</label>
                                    <input type="text" class="form-control" name="point_1" id="update-question-point-1">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 2</label>
                                    <input type="text" class="form-control" name="answer_2" id="update-question-answer-2">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 2</label>
                                    <input type="text" class="form-control" name="point_2" id="update-question-point-2">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 3</label>
                                    <input type="text" class="form-control" name="answer_3" id="update-question-answer-3">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 3</label>
                                    <input type="text" class="form-control" name="point_3" id="update-question-point-3">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 4</label>
                                    <input type="text" class="form-control" name="answer_4" id="update-question-answer-4">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 4</label>
                                    <input type="text" class="form-control" name="point_4" id="update-question-point-4">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jawaban 5</label>
                                    <input type="text" class="form-control" name="answer_5" id="update-question-answer-5">
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Point 5</label>
                                    <input type="text" class="form-control" name="point_5" id="update-question-point-5">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-question">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endsection

@section('js-script')
    <script>

        var url_select2 = "{{ route('admin.question_group.data.select2') }}";

        $(function () {
            "use strict";

            @foreach ($course->exams as $exam)
                getQuestion{{ $exam->id }}();
            @endforeach

            initSelect2Form();

            @can ('question.create')
                $("#form-add-question").on("submit", function(e) {
                    e.preventDefault();
                    addQuestion();
                });
            @endcan

            @can ('question.create')
                $("#form-update-question").on("submit", function(e) {
                    e.preventDefault();
                    updateQuestion();
                });
            @endcan

        });

        @foreach ($course->exams as $exam)
            async function getQuestion{{ $exam->id }}()
            {
                $("#question-{{ $exam->id }}").dataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.question.data', ['exam' => $exam]) }}",
                    destroy: true,
                    columns: [
                        { data: 'DT_RowIndex' },
                        { data: 'question' },
                        { data: 'question_type' },
                        { data: 'question_group' },
                        { data: 'action' },
                    ]
                });
            }
        @endforeach

        async function openModalAdd(object)
        {
            var id   = $(object).data('id'),
                name = $(object).data('name');

            $('#modal-add-question').modal('show');
            setTimeout(() => {
                $('#add-question-question').focus();
                $('#add-question-exam-id').val(id);
                $('#add-question-exam-id-view').val(name);
            },500);

            url_select2 = "{{ route('admin.question_group.data.select2') }}/"+id;
        }

        async function initSelect2Form()
        {
            $('.select2-form').select2({
                width: '100%',
                placeholder: "Pilih Grup Soal",
                // minimumInputLength: 1,
                ajax: {
                    url: function() {
                        return url_select2;
                    },
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        @can ('question.create')
            async function addQuestion()
            {
                var formData = $("#form-add-question").serialize();

                $.ajax({
                    url: "{{ route('admin.question.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-question").addClass('btn-progress');
                        $("#btn-add-question").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-question").removeClass('btn-progress');
                        $("#btn-add-question").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-question")[0].reset();
                            $('#modal-add-question').modal('hide');
                            @foreach ($course->exams as $exam)
                                getQuestion{{ $exam->id }}();
                            @endforeach
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('question.update')
            async function getUpdateQuestion(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-question').modal('show');
                $('#form-update-question')[0].reset();

                $.ajax({
                    url: "{{ route('admin.question.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-question").addClass('btn-progress');
                        $("#btn-update-question").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-question").removeClass('btn-progress');
                        $("#btn-update-question").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-question-id').val(result['data']['question']['id']);
                        $('#update-question-question-type').val(result['data']['question']['question_type']);
                        $('#update-question-question').val(result['data']['question']['question']);
                        $('#update-question-answer-1').val(result['data']['question']['answer_1']);
                        $('#update-question-point-1').val(result['data']['question']['point_1']);
                        $('#update-question-answer-2').val(result['data']['question']['answer_2']);
                        $('#update-question-point-2').val(result['data']['question']['point_2']);
                        $('#update-question-answer-3').val(result['data']['question']['answer_3']);
                        $('#update-question-point-3').val(result['data']['question']['point_3']);
                        $('#update-question-answer-4').val(result['data']['question']['answer_4']);
                        $('#update-question-point-4').val(result['data']['question']['point_4']);
                        $('#update-question-answer-5').val(result['data']['question']['answer_5']);
                        $('#update-question-point-5').val(result['data']['question']['point_5']);
                        $('#update-question-exam-id').val(result['data']['exam']['name']);
                        if(result['data']['question_group'] != null){
                            $('#update-question-question-group-id').val(result['data']['question_group']['name']);
                        }
                        setTimeout(() => {
                            $('#update-question-question').focus()
                        }, 1);
                    }
                });
            }

            async function updateQuestion()
            {
                var formData = $("#form-update-question").serialize();

                $.ajax({
                    url: "{{ route('admin.question.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-question").addClass('btn-progress');
                        $("#btn-update-question").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-question").removeClass('btn-progress');
                        $("#btn-update-question").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-question")[0].reset();
                            $('#modal-update-question').modal('hide');
                            @foreach ($course->exams as $exam)
                                getQuestion{{ $exam->id }}();
                            @endforeach
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('question.delete')
            async function deleteQuestion(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus pertanyaan?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.question.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    @foreach ($course->exams as $exam)
                                        getQuestion{{ $exam->id }}();
                                    @endforeach
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
