@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Grup Soal</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Grup Soal</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card mb-0">
                    <div class="card-header">
                        <h4>Filter</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group mb-0">
                            <select name="filter" id="filter" class="form-control select2-filter" onchange="getQuestionGroups(this.value)"></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Grup Soal</h4>
                        @can ('question_group.create')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-question-group').modal('show');setTimeout(() => {$('#add-question-group-name').focus()},500)" tooltip="Tambah Grup Soal"><i class="fas fa-plus"></i> Tambah Grup Soal</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="question-group-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Materi</th>
                                        <th>Nama</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can ('question_group.create')
        <div class="modal fade" role="dialog" id="modal-add-question-group">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Grup Soal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-question-group">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <select name="exam_id" id="add-question-group-exam-id" class="form-control select2-form"></select>
                                </div>
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="add-question-group-name" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-question-group">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('question_group.update')
        <div class="modal fade" role="dialog" id="modal-update-question-group">
            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Grup Soal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-question-group">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-question-group-id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label>Mapel</label>
                                    <input type="text" class="form-control" name="exam_id" id="update-question-group-exam-id" readonly>
                                </div>
                                <div class="form-group col-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="update-question-group-name" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-question-group">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getQuestionGroups();

            initSelect2Filter();
            initSelect2Form();

            @can ('question_group.create')
                $("#form-add-question-group").on("submit", function(e) {
                    e.preventDefault();
                    addQuestionGroup();
                });
            @endcan

            @can ('question_group.update')
                $("#form-update-question-group").on("submit", function(e) {
                    e.preventDefault();
                    updateQuestionGroup();
                });
            @endcan
        });

        async function getQuestionGroups(filter = '')
        {
            $("#question-group-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.question_group.data') }}/"+filter,
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'exam_name' },
                    { data: 'name' },
                    { data: 'action' },
                ]
            });
        }

        async function initSelect2Filter()
        {
            $('.select2-filter').select2({
                width: '100%',
                placeholder: "Filter",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.exam.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        async function initSelect2Form()
        {
            $('.select2-form').select2({
                width: '100%',
                placeholder: "Pilih Mapel",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.exam.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        @can ('question_group.create')
            async function addQuestionGroup()
            {
                var formData = $("#form-add-question-group").serialize();

                $.ajax({
                    url: "{{ route('admin.question_group.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-question-group").addClass('btn-progress');
                        $("#btn-add-question-group").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-question-group").removeClass('btn-progress');
                        $("#btn-add-question-group").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-question-group")[0].reset();
                            $('#modal-add-question-group').modal('hide');
                            getQuestionGroups();
                            $('#filter').removeAttr('selected');
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('question_group.update')
            async function getUpdateQuestionGroup(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-question-group').modal('show');
                $('#form-update-question-group')[0].reset();

                $.ajax({
                    url: "{{ route('admin.question_group.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-question-group").addClass('btn-progress');
                        $("#btn-update-question-group").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-question-group").removeClass('btn-progress');
                        $("#btn-update-question-group").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-question-group-id').val(result['data']['question_group']['id']);
                        $('#update-question-group-name').val(result['data']['question_group']['name']);
                        $('#update-question-group-exam-id').val(result['data']['exam']['name']);
                        setTimeout(() => {
                            $('#update-question-group-name').focus()
                        }, 1);
                    }
                });
            }

            async function updateQuestionGroup()
            {
                var formData = $("#form-update-question-group").serialize();

                $.ajax({
                    url: "{{ route('admin.question_group.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-question-group").addClass('btn-progress');
                        $("#btn-update-question-group").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-question-group").removeClass('btn-progress');
                        $("#btn-update-question-group").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-question-group")[0].reset();
                            $('#modal-update-question-group').modal('hide');
                            getQuestionGroups();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('question_group.delete')
            async function deleteQuestionGroup(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus grup soal?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.question_group.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    getQuestionGroups();
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
