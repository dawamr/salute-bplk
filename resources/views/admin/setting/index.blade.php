@extends('templates.admin')

@section('content')
<div class="section-header">
    <h1>Pengaturan</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
        <div class="breadcrumb-item">Pengaturan</div>
    </div>
</div>
@endsection
