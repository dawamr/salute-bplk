@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Pelatihan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item">Pelatihan</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Pelatihan</h4>
                        @can ('training.create')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-training').modal('show');setTimeout(() => {$('#add-training-name').focus()},500)" tooltip="Tambah Pelatihan"><i class="fas fa-plus"></i> Tambah Pelatihan</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="training-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Kejuruan</th>
                                        <th>Program</th>
                                        <th>Tahun</th>
                                        <th>Tahap</th>
                                        <th>Kelas</th>
                                        <th>Tipe</th>
                                        <th>Kode</th>
                                        <th width="150">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can ('training.create')
        <div class="modal fade" role="dialog" id="modal-add-training">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Pelatihan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-training">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Kejuruan</label>
                                    <select name="vocational_id" id="add-training-vocational-id" class="form-control select2-form-vocational"></select>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Program</label>
                                    <select name="program_id" id="add-training-program-id" class="form-control select2-form-program"></select>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control datemask" name="year" id="add-training-year" placeholder="{{ \Carbon\Carbon::now()->year }}" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Tahap</label>
                                    <input type="text" class="form-control field-step" name="step" id="add-training-step" required autofocus>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Kelas</label>
                                    <input type="text" class="form-control field-class" name="class" id="add-training-class" required>
                                </div>
                                <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                    <label>Jenis Pelatihan</label>
                                    <div class="selectgroup w-100">
                                        <label class="selectgroup-item">
                                            <input type="radio" name="training_type" id="add-training-type-mn" value="stay_overnight" class="selectgroup-input" checked="" required>
                                            <span class="selectgroup-button">Menginap</span>
                                        </label>
                                        <label class="selectgroup-item">
                                            <input type="radio" name="training_type" id="add-training-type-pn" value="go_home" class="selectgroup-input" required>
                                            <span class="selectgroup-button">Pulang</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-training">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    @can ('training.update')
        <div class="modal fade" role="dialog" id="modal-update-training">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Perbarui Izin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-update-training">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="" id="update-training-id">
                        <div class="modal-body">
                            <div class="row">
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Kejuruan</label>
                                        <input type="text" class="form-control" name="vocational_id" id="update-training-vocational-id" readonly>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Program</label>
                                        <input type="text" class="form-control" name="program_id" id="update-training-program-id" readonly>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Tahun</label>
                                        <input type="text" class="form-control datemask" name="year" id="update-training-year" placeholder="{{ \Carbon\Carbon::now()->year }}" required autofocus>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Tahap</label>
                                        <input type="text" class="form-control field-step" name="step" id="update-training-step" required autofocus>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Kelas</label>
                                        <input type="text" class="form-control field-class" name="class" id="update-training-class" required>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                        <label>Jenis Pelatihan</label>
                                        <div class="selectgroup w-100">
                                            <label class="selectgroup-item">
                                                <input type="radio" name="training_type" id="update-training-type-mn" value="stay_overnight" class="selectgroup-input" checked="" required>
                                                <span class="selectgroup-button">Menginap</span>
                                            </label>
                                            <label class="selectgroup-item">
                                                <input type="radio" name="training_type" id="update-training-type-pn" value="go_home" class="selectgroup-input" required>
                                                <span class="selectgroup-button">Pulang</span>
                                            </label>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-update-training">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/cleave-js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        $(function () {
            "use strict";

            getTrainings();

            initSelect2Form();

            var cleaveD = new Cleave('.datemask', {
                date: true,
                datePattern: ['Y']
            });

            var cleaveI = new Cleave('.field-step', {
                numeral: true,
                delimiter: '',
                blocks: [10],
                uppercase: true
            });

            var cleaveI = new Cleave('.field-class', {
                numeral: true,
                delimiter: '',
                blocks: [10],
            });

            @can ('training.create')
                $("#form-add-training").on("submit", function(e) {
                    e.preventDefault();
                    addTraining();
                });
            @endcan

            @can ('training.update')
                $("#form-update-training").on("submit", function(e) {
                    e.preventDefault();
                    updateTraining();
                });
            @endcan
        });

        async function getTrainings()
        {
            $("#training-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.training.data') }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'vocational_name' },
                    { data: 'program_name' },
                    { data: 'year' },
                    { data: 'step' },
                    { data: 'class' },
                    { data: 'training_type' },
                    { data: 'code' },
                    { data: 'action' },
                ]
            });
        }

        async function initSelect2Form()
        {
            $('.select2-form-vocational').select2({
                width: '100%',
                placeholder: "Pilih Kejuruan",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.vocational.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })

            $('.select2-form-program').select2({
                width: '100%',
                placeholder: "Pilih Program",
                // minimumInputLength: 1,
                ajax: {
                    url: "{{ route('admin.program.data.select2') }}",
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        async function copyCode(object)
        {
            var code = $(object).data('code');
            var copiedNow = false;

            var textArea = document.createElement( "textarea" );
            textArea.value = code;
            document.body.appendChild( textArea );

            textArea.select();

            try{
                var clipboard = document.execCommand('copy');

                $(object).attr('tooltip', 'Berhasil Disalin!');

                if(copiedNow){
                    clearTimeout(copiedNow);
                }

                copiedNow = setTimeout(() => {
                    $(object).attr('tooltip', 'Klik untuk menyalin!');
                }, 3000);
            }catch(err){
                // console.log('Oops, unable to copy');
            }

            document.body.removeChild( textArea );
        }

        @can ('training.create')
            async function addTraining()
            {
                var formData = $("#form-add-training").serialize();

                $.ajax({
                    url: "{{ route('admin.training.store') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-add-training").addClass('btn-progress');
                        $("#btn-add-training").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-add-training").removeClass('btn-progress');
                        $("#btn-add-training").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-add-training")[0].reset();
                            $('#modal-add-training').modal('hide');
                            getTrainings();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can ('training.update')
            async function getUpdateTraining(obj)
            {
                var id = $(obj).data('id');

                $('#modal-update-training').modal('show');
                $('#form-update-training')[0].reset();

                $.ajax({
                    url: "{{ route('admin.training.edit') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "POST",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {
                        $("#btn-update-training").addClass('btn-progress');
                        $("#btn-update-training").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-training").removeClass('btn-progress');
                        $("#btn-update-training").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        $('#update-training-id').val(result['data']['training']['id']);
                        $('#update-training-year').val(result['data']['training']['year']);
                        $('#update-training-step').val(result['data']['training']['step']);
                        $('#update-training-class').val(result['data']['training']['class']);
                        if(result['data']['training']['training_type'] == 'stay_overnight'){
                            $('#update-training-type-mn').prop('checked', true);
                        }
                        if(result['data']['training']['training_type'] == 'go_home'){
                            $('#update-training-type-pn').prop('checked', true);
                        }

                        $('#update-training-vocational-id').val(result['data']['vocational']['name']);
                        $('#update-training-program-id').val(result['data']['program']['name']);
                    }
                });
            }

            async function updateTraining()
            {
                var formData = $("#form-update-training").serialize();

                $.ajax({
                    url: "{{ route('admin.training.update') }}",
                    type: "POST",
                    dataType: "json",
                    data: formData,
                    beforeSend() {
                        $("#btn-update-training").addClass('btn-progress');
                        $("#btn-update-training").attr('disabled', 'disabled');
                        $("input").attr('disabled', 'disabled');
                        $("select").attr('disabled', 'disabled');
                        $("button").attr('disabled', 'disabled');
                    },
                    complete() {
                        $("#btn-update-training").removeClass('btn-progress');
                        $("#btn-update-training").removeAttr('disabled', 'disabled');
                        $("input").removeAttr('disabled', 'disabled');
                        $("select").removeAttr('disabled', 'disabled');
                        $("button").removeAttr('disabled', 'disabled');
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            $("#form-update-training")[0].reset();
                            $('#modal-update-training').modal('hide');
                            getTrainings();
                        }

                        notification(result['status'], result['title'], result['msg']);
                    }
                });
            }
        @endcan

        @can('training.delete')
            async function deleteTraining(object)
            {
                var id = $(object).data('id');
                swal({
                    title: 'Anda yakin menghapus pelatihan?',
                    text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal({
                            title: 'Loading..!',
                            text: 'Tunggu sebentar..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            buttons:false,
                        })
                        $.ajax({
                            url: "{{ route('admin.training.destroy') }}",
                            type: "POST",
                            dataType: "json",
                            data: {
                                "id": id,
                                "_method": "DELETE",
                                "_token": "{{ csrf_token() }}"
                            },
                            success : function(result) {
                                if(result['status'] == 'success'){
                                    getTrainings();
                                }
                                swalNotification(result['status'], result['msg']);
                            }
                        });
                    } else {
                        swal('Data Anda Aman!', {icon:'success'});
                    }
                });

            }
        @endcan
    </script>
@endsection
