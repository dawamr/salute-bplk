@extends('templates.admin')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
    <div class="section-header">
        <h1>Pelatihan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('admin.training') }}">Pelatihan</a></div>
            <div class="breadcrumb-item">{{ $training->code }}</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-user-friends"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Peserta</h4>
                        </div>
                        <div class="card-body">
                            {{ $training->users->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-chalkboard-teacher"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pengajar</h4>
                        </div>
                        <div class="card-body">
                            {{ $training->instructors->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-file"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Soal</h4>
                        </div>
                        <div class="card-body">
                            {{ $training->courses->count() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-circle"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Jawaban Masuk</h4>
                        </div>
                        <div class="card-body">
                            {{ $training->answers->count() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Peserta</h4>
                        @can ('training.manage')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" data-id="{{ $training->id }}" data-name="Peserta" data-intern="participant" onclick="openModal(this);" tooltip="Tambah Peserta"><i class="fas fa-plus"></i> Tambah Peserta</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="training-participant-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Nama</th>
                                        <th>Nama Pengguna</th>
                                        <th>Email</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Umur</th>
                                        <th>Pendidikan</th>
                                        <th>Pekerjaan</th>
                                        <th width="50">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Pengajar</h4>
                        @can ('training.manage')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" data-id="{{ $training->id }}" data-name="Pengajar" data-intern="instructor" onclick="openModal(this);" tooltip="Tambah Pengajar"><i class="fas fa-plus"></i> Tambah Pengajar</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="training-instructor-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Nama</th>
                                        <th width="50">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Daftar Mapel</h4>
                        @can ('training.manage')
                            <div class="card-header-action">
                                <a href="javascript:void(0)" class="btn btn-primary" data-id="{{ $training->id }}" data-name="Mapel" data-intern="course" onclick="openModal(this);" tooltip="Tambah Soal"><i class="fas fa-plus"></i> Tambah Soal</a>
                            </div>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="training-course-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Nama</th>
                                        <th width="50">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can ('training.manage')
        <div class="modal fade" role="dialog" id="modal-add-training-participant">
            <div class="modal-dialog modal-dialog-centered modal-mg" role="document">
                <div class="modal-content" id="modal-content-view">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-add-training-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-training-participant">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label id="add-training-label"></label>
                                    <select name="user_id[]" id="add-training-user-id" class="form-control select2-form" multiple></select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-training-participant">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" role="dialog" id="modal-add-training-instructor">
            <div class="modal-dialog modal-dialog-centered modal-mg" role="document">
                <div class="modal-content" id="modal-content-view">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-add-training-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-training-instructor">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label id="add-training-label"></label>
                                    <select name="instructor_id[]" id="add-training-instructor-id" class="form-control select2-form" multiple></select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-training-instructor">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" role="dialog" id="modal-add-training-course">
            <div class="modal-dialog modal-dialog-centered modal-mg" role="document">
                <div class="modal-content" id="modal-content-view">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-add-training-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="javascript:void(0)" id="form-add-training-course">
                        @csrf
                        <input type="hidden" name="training_id" value="{{ $training->id }}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label id="add-training-label"></label>
                                    <select name="course_id[]" id="add-training-course-id" class="form-control select2-form" multiple></select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btn-add-training-course">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('js-library')
    <script src="{{ asset('assets/modules/cleave-js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
    <script>
        var url_select2 = "";

        $(function () {
            "use strict";

            getTrainingParticipants();
            getTrainingInstructors();
            getTrainingCourses();

            @can ('training.manage')
                $("#form-add-training-participant").on("submit", function(e) {
                    e.preventDefault();
                    addTrainingParticipant();
                });
                $("#form-add-training-instructor").on("submit", function(e) {
                    e.preventDefault();
                    addTrainingInstructor();
                });
                $("#form-add-training-course").on("submit", function(e) {
                    e.preventDefault();
                    addTrainingCourse();
                });
            @endcan
        });

        async function getTrainingParticipants()
        {
            $("#training-participant-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.training.data.participant', ['training' => $training]) }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'name' },
                    { data: 'username' },
                    { data: 'email' },
                    { data: 'gender' },
                    { data: 'date_of_birth' },
                    { data: 'education' },
                    { data: 'job' },
                    { data: 'action' },
                ]
            });
        }

        async function getTrainingInstructors()
        {
            $("#training-instructor-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.training.data.instructor', ['training' => $training]) }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'name' },
                    { data: 'action' },
                ]
            });
        }

        async function getTrainingCourses()
        {
            $("#training-course-list").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.training.data.course', ['training' => $training]) }}",
                destroy: true,
                columns: [
                    { data: 'DT_RowIndex' },
                    { data: 'name' },
                    { data: 'action' },
                ]
            });
        }

        async function openModal(object)
        {
            var id   = $(object).data('id'),
                name = $(object).data('name'),
                intern = $(object).data('intern');

            $('#modal-add-training-'+intern).modal('show');

            initSelect2Form();

            if(intern == 'participant'){
                url_select2 = "{{ route('admin.training.data.select2.participant', ['training' => $training]) }}";
            }
            if(intern == 'instructor'){
                url_select2 = "{{ route('admin.training.data.select2.instructor', ['training' => $training]) }}";
            }
            if(intern == 'course'){
                url_select2 = "{{ route('admin.training.data.select2.course', ['training' => $training]) }}";
            }
        }

        async function initSelect2Form()
        {
            $('.select2-form').select2({
                width: '100%',
                placeholder: "Pilih Kejuruan",
                // minimumInputLength: 1,
                ajax: {
                    url: function() {
                        return url_select2;
                    },
                    type: "get",
                    dataType: "json",
                    quietMillis: 50,
                    delay: 250,
                    data: function (term) {
                        text = term.term ? term.term : '';
                        query = {
                          _type: term._type,
                          term: text,
                        };
                        return {
                            data: query,
                        };
                    },
                    processResults : function(data) {;
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        }
                    }
                },
            })
        }

        async function addTrainingParticipant()
        {
            var formData = $("#form-add-training-participant").serialize();

            $.ajax({
                url: "{{ route('admin.training.store.participant', ['training' => $training]) }}",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("#btn-add-training-participant").addClass('btn-progress');
                    $("#btn-add-training-participant").attr('disabled', 'disabled');
                    $("input").attr('disabled', 'disabled');
                    $("select").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                },
                complete() {
                    $("#btn-add-training-participant").removeClass('btn-progress');
                    $("#btn-add-training-participant").removeAttr('disabled', 'disabled');
                    $("input").removeAttr('disabled', 'disabled');
                    $("select").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        $("#form-add-training-participant")[0].reset();
                        $('#modal-add-training').modal('hide');
                        getTrainingParticipants();
                        getTrainingInstructors();
                        getTrainingCourses();
                    }

                    notification(result['status'], result['title'], result['msg']);
                }
            });
        }

        async function addTrainingInstructor()
        {
            var formData = $("#form-add-training-instructor").serialize();

            $.ajax({
                url: "{{ route('admin.training.store.instructor', ['training' => $training]) }}",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("#btn-add-training-instructor").addClass('btn-progress');
                    $("#btn-add-training-instructor").attr('disabled', 'disabled');
                    $("input").attr('disabled', 'disabled');
                    $("select").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                },
                complete() {
                    $("#btn-add-training-instructor").removeClass('btn-progress');
                    $("#btn-add-training-instructor").removeAttr('disabled', 'disabled');
                    $("input").removeAttr('disabled', 'disabled');
                    $("select").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        $("#form-add-training-instructor")[0].reset();
                        $('#modal-add-training-instructor').modal('hide');
                        getTrainingParticipants();
                        getTrainingInstructors();
                        getTrainingCourses();
                    }

                    notification(result['status'], result['title'], result['msg']);
                }
            });
        }

        async function addTrainingCourse()
        {
            var formData = $("#form-add-training-course").serialize();

            $.ajax({
                url: "{{ route('admin.training.store.course', ['training' => $training]) }}",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("#btn-add-training-course").addClass('btn-progress');
                    $("#btn-add-training-course").attr('disabled', 'disabled');
                    $("input").attr('disabled', 'disabled');
                    $("select").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                },
                complete() {
                    $("#btn-add-training-course").removeClass('btn-progress');
                    $("#btn-add-training-course").removeAttr('disabled', 'disabled');
                    $("input").removeAttr('disabled', 'disabled');
                    $("select").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        $("#form-add-training-course")[0].reset();
                        $('#modal-add-training-course').modal('hide');
                        getTrainingParticipants();
                        getTrainingInstructors();
                        getTrainingCourses();
                    }

                    notification(result['status'], result['title'], result['msg']);
                }
            });
        }
    </script>
@endsection
