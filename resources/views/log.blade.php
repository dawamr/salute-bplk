@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Log</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ route('dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Log</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">Logs</h2>
    <p class="section-lead">Semua aktifitas tersimpan di sini.</p>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Log</h4>
                </div>
                <div class="card-body">
                    {{-- <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent"> --}}
                        <div class="table-responsive">
                            <table class="table table-striped" id="log-list">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10">
                                            #
                                        </th>
                                        <th>Auth</th>
                                        <th>User (username)</th>
                                        <th>Judul</th>
                                        <th>Pesan</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection


@section('js-script')
<script>

    $(function () {
        "use strict";

        getLogList();
    });

    async function getLogList()
    {
        $("#log-list").dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('log.all') }}",
            destroy: true,
            columns: [
                { data: 'id' },
                { data: 'auth' },
                { data: 'user' },
                { data: 'title' },
                { data: 'message' },
                { data: 'created_at' },
            ],
            order: [0, 'desc']
        });
    }
</script>
@endsection
