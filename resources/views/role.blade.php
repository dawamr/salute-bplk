@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Role</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">Role</div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Role</h4>
                    @can ('role.create')
                        <div class="card-header-action">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-role').modal('show');setTimeout(() => { $('#add-role-name').focus() }, 500);" tooltip="Tambah Role"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="role-list">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">
                                        #
                                    </th>
                                    <th>Nama</th>
                                    <th>User Default</th>
                                    <th width="150">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@can('role.manage')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-manage-role">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Manage Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-manage-role">
                    @csrf
                    @method('PUT')
                    <div class="modal-body" id="view-manage-role">

                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-manage-role">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('role.create')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-role">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-add-role">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" id="add-role-name" required autofocus>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-add-role">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('role.update')
<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-role">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="javascript:void(0)" id="form-update-role">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="" id="update-role-id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="name" id="update-role-name" required autofocus>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" id="btn-update-role">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endcan
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
<script>

    $(function () {
        "use strict";

        getRoleList();

        @can ('role.manage')
            $("#form-manage-role").on("submit", function(e) {
                e.preventDefault();
                manageRole();
            });
        @endcan

        @can ('role.create')
        $("#form-add-role").on("submit", function(e) {
            e.preventDefault();
            addRole();
        });
        @endcan

        @can ('role.update')
        $("#form-update-role").on("submit", function(e) {
            e.preventDefault();
            updateRole();
        });
        @endcan
    });

    async function getRoleList()
    {
        $("#role-list").dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('role.all') }}",
            destroy: true,
            columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'user_default' },
                { data: 'action' },
            ]
        });
    }

    @can('role.manage')
    async function getManageRole(obj)
    {
        var id = $(obj).data('id');
        $('#modal-manage-role').modal('show');
        $("#view-manage-role").html('<h4 class="text-center my-4">Loading . . .</h4>');

        $.ajax({
            url: "{{ route('role.manage.data') }}",
            type: "POST",
            dataType: "html",
            data: {
                "id": id,
                "_method": "POST",
                "_token": "{{ csrf_token() }}"
            },
            beforeSend() {
                $("#btn-manage-role").addClass('btn-progress');
                $("#btn-manage-role").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-manage-role").removeAttr('disabled', 'disabled');
                $("#btn-manage-role").removeClass('btn-progress');
            },
            success : function(result) {
                $("#view-manage-role").html(result);
            }
        });
    }
    async function manageRole()
    {
        var formData = $("#form-manage-role").serialize();

        $.ajax({
            url: "{{ route('role.manage') }}",
            type: "POST",
            dataType: "json",
            data: formData,
            beforeSend() {
                $("#btn-manage-role").addClass('btn-progress');
                $("input").attr('disabled', 'disabled');
                $("#btn-manage-role").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("input").removeAttr('disabled', 'disabled');
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-manage-role").removeAttr('disabled', 'disabled');
                $("#btn-manage-role").removeClass('btn-progress');
            },
            success : function(result) {
                if(result['status'] == 'success'){
                    $("#form-manage-role")[0].reset();
                    $('#modal-manage-role').modal('hide');
                    getRoleList();
                }

                notification(result['status'], result['title'], result['msg']);
            }
        });
    }
    @endcan

    @can ('role.create')
    async function addRole()
    {
        var formData = $("#form-add-role").serialize();

        $.ajax({
            url: "{{ route('role.store') }}",
            type: "POST",
            dataType: "json",
            data: formData,
            beforeSend() {
                $("#btn-add-role").addClass('btn-progress');
                $("input").attr('disabled', 'disabled');
                $("#btn-add-role").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("input").removeAttr('disabled', 'disabled');
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-add-role").removeAttr('disabled', 'disabled');
                $("#btn-add-role").removeClass('btn-progress');
            },
            success : function(result) {
                if(result['status'] == 'success'){
                    $("#form-add-role")[0].reset();
                    $('#modal-add-role').modal('hide');
                    getRoleList();
                }

                notification(result['status'], result['title'], result['msg']);
            }
        });
    }
    @endcan

    @can ('role.update')
    async function editRole(obj)
    {
        var id = $(obj).data('id');
        $('#modal-update-role').modal('show');
        $('#form-update-role')[0].reset();

        $.ajax({
            url: "{{ route('role.edit') }}",
            type: "POST",
            dataType: "json",
            data: {
                "id": id,
                "_method": "POST",
                "_token": "{{ csrf_token() }}"
            },
            beforeSend() {
                $("#btn-update-role").addClass('btn-progress');
                $("input").attr('disabled', 'disabled');
                $("#btn-update-role").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("input").removeAttr('disabled', 'disabled');
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-update-role").removeAttr('disabled', 'disabled');
                $("#btn-update-role").removeClass('btn-progress');
            },
            success : function(result) {
                $('#update-role-id').val(result['data']['id']);
                $('#update-role-name').val(result['data']['name']);
                setTimeout(() => {
                    $('#update-role-name').focus()
                }, 50);
            }
        });
    }

    async function updateRole()
    {
        var formData = $("#form-update-role").serialize();

        $.ajax({
            url: "{{ route('role.update') }}",
            type: "POST",
            dataType: "json",
            data: formData,
            beforeSend() {
                $("#btn-update-role").addClass('btn-progress');
                $("input").attr('disabled', 'disabled');
                $("#btn-update-role").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("input").removeAttr('disabled', 'disabled');
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-update-role").removeAttr('disabled', 'disabled');
                $("#btn-update-role").removeClass('btn-progress');
            },
            success : function(result) {
                if(result['status'] == 'success'){
                    $("#form-update-role")[0].reset();
                    $('#modal-update-role').modal('hide');
                    getRoleList();
                }

                notification(result['status'], result['title'], result['msg']);
            }
        });
    }

    async function setDefaultRole(object)
    {
        var id = $(object).data('id');
        swal({
            title: 'Update Default User',
            text: 'Anda yakin mengubah default user role?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal({
                    title: 'Loading..!',
                    text: 'Tunggu sebentar..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons:false,
                })
                $.ajax({
                    url: "{{ route('role.set_user_default') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "PUT",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {

                    },
                    complete() {
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            getRoleList();
                        }
                        swalNotification(result['status'], result['msg']);
                    }
                });
            } else {
                swal('Data Anda Masih Sama!', {icon:'success'});
            }
        });
    }
    @endcan

    @can('role.delete')
    async function deleteRole(object)
    {
        var id = $(object).data('id');
        swal({
            title: 'Anda yakin?',
            text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal({
                    title: 'Loading..!',
                    text: 'Tunggu sebentar..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons:false,
                })
                $.ajax({
                    url: "{{ route('role.destroy') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": id,
                        "_method": "DELETE",
                        "_token": "{{ csrf_token() }}"
                    },
                    beforeSend() {

                    },
                    complete() {
                    },
                    success : function(result) {
                        if(result['status'] == 'success'){
                            getRoleList();
                        }
                        swalNotification(result['status'], result['msg']);
                    }
                });
            } else {
                swal('Data Anda Aman!', {icon:'success'});
            }
        });

    }
    @endcan

    async function swalNotification(status, message)
    {
        swal(message, {
            icon: status,
        });
    }

    async function notification(status, titleToast, messageToast)
    {
        if(status == 'success'){
            iziToast.success({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }
        if(status == 'warning'){
            iziToast.warning({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }
        if(status == 'error'){
            iziToast.error({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }

    }
</script>
@endsection

