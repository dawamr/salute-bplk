@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Pengaturan</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">Pengaturan</div>
    </div>
</div>

<div class="section-body">
    <h2 class="section-title">Semua tentang pengaturan umum</h2>
    <p class="section-lead">
        Anda dapat menyesuaikan semua pengaturan umum di sini
    </p>

    <div id="output-status"></div>
    <div class="row">
        <div class="col-md-12">
            <form action="javascript:void(0)" method="post" id="form-update-setting" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card" id="settings-card">
                    <div class="card-header">
                        <h4>Pengaturan Umum</h4>
                    </div>
                    <div class="card-body">
                        <p class="text-muted">Pengaturan umum seperti, judul situs, deskripsi situs dan sebagainya.</p>
                        <div class="form-group row align-items-center">
                            <label for="update-app-name" class="form-control-label col-sm-3 text-md-right">Nama Aplikasi</label>
                            <div class="col-sm-6 col-md-9">
                                <input type="text" name="app_name" class="form-control" id="update-app-name" value="{{ $app_name }}">
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="app-description" class="form-control-label col-sm-3 text-md-right">Deskripsi Aplikasi</label>
                            <div class="col-sm-6 col-md-9">
                                <textarea class="form-control" name="app_description" id="app-description">{{ $app_description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="app-version" class="form-control-label col-sm-3 text-md-right">Versi Aplikasi</label>
                            <div class="col-sm-6 col-md-9">
                                <input class="form-control" name="app_version" id="app-version" type="text" value="{{ $app_version }}">
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label class="form-control-label col-sm-3 text-md-right">Logo Aplikasi</label>
                            <div class="col-sm-6 col-md-9">
                                <div id="update-favicon-preview" class="image-preview">
                                    <label for="update-favicon-upload" id="update-favicon-label">Pilih Gambar</label>
                                    <input type="file" name="app_logo" id="update-favicon-upload" />
                                </div>
                                @if ($app_logo != 'uploads/site/logo/default.png')
                                <div class="custom-control custom-checkbox mt-2">
                                    <input type="checkbox" class="custom-control-input" id="null-app-logo" name="null_app_logo" value="1">
                                    <label class="custom-control-label" for="null-app-logo">Kosongkan Logo Aplikasi</label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-whitesmoke text-md-right">
                        <button type="submit" class="btn btn-primary" id="btn-update-setting">Simpan Perubahan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
@endsection

@section('js-script')
<script>

    $(function () {
        "use strict";

        imagePreview();
        $('#update-favicon-preview').css('background', `url('{{ asset($app_logo) }}') center center / cover transparent`);

        $("#form-update-setting").on("submit", function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            updateSetting(formData);
        });
    });

    async function imagePreview()
    {
        $.uploadPreview({
            input_field: "#update-favicon-upload",   // Default: .image-upload
            preview_box: "#update-favicon-preview",  // Default: .image-preview
            label_field: "#update-favicon-label",    // Default: .image-label
            label_default: "Choose File",   // Default: Choose File
            label_selected: "Change File",  // Default: Change File
            no_label: false,                // Default: false
            success_callback: null          // Default: null
        });
    }

    async function updateSetting(formData)
    {
        $.ajax({
            url: "{{ route('setting.update') }}",
            type: "POST",
            dataType: "json",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend() {
                $("#btn-update-setting").addClass('btn-progress');
                $("textarea").attr('disabled', 'disabled');
                $("input").attr('disabled', 'disabled');
                $("#btn-update-setting").attr('disabled', 'disabled');
                $("button").attr('disabled', 'disabled');
            },
            complete() {
                $("textarea").removeAttr('disabled', 'disabled');
                $("input").removeAttr('disabled', 'disabled');
                $("button").removeAttr('disabled', 'disabled');
                $("#btn-update-setting").removeAttr('disabled', 'disabled');
                $("#btn-update-setting").removeClass('btn-progress');
            },
            success : function(result) {
                notification(result['status'], result['title'], result['msg']);
            }
        });
    }

    async function notification(status, titleToast, messageToast)
    {
        if(status == 'success'){
            iziToast.success({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }
        if(status == 'warning'){
            iziToast.warning({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }
        if(status == 'error'){
            iziToast.error({
                title: titleToast,
                message: messageToast,
                position: 'topRight'
            });
        }

    }
</script>
@endsection

