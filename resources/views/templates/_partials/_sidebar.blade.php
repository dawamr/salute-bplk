<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.dashboard') }}">{{ $app_name }}</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard') }}">{{ $app_name_small }}</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Menu</li>
            <li class="{{ Request::route()->getName() == 'admin.dashboard' ? 'active' : '' }}"><a href="{{ route("admin.dashboard") }}" class="nav-link"><i class="fas fa-fire"></i><span>Beranda</span></a></li>
            @can('training.view')
                <li class="{{ Request::route()->getPrefix() == 'admin/trainings' ? 'active' : '' }}"><a href="{{ route("admin.training") }}" class="nav-link"><i class="far fa-gem"></i><span>Pelatihan</span></a></li>
            @endcan
            @can('question.view')
                <li class="{{ Request::route()->getPrefix() == 'admin/questions' ? 'active' : '' }}"><a href="{{ route("admin.question") }}" class="nav-link"><i class="far fa-file"></i><span>Soal</span></a></li>
            @endcan
            <li class="menu-header">Master Data</li>
            @can('participant.view')
                <li class="{{ Request::route()->getName() == 'admin.participant' ? 'active' : '' }}"><a href="{{ route("admin.participant") }}" class="nav-link"><i class="fas fa-user-friends"></i><span>Peserta</span></a></li>
            @endcan
            @can('instructor.view')
                <li class="{{ Request::route()->getName() == 'admin.instructor' ? 'active' : '' }}"><a href="{{ route("admin.instructor") }}" class="nav-link"><i class="fas fa-chalkboard-teacher"></i><span>Pengajar</span></a></li>
            @endcan
            @if(auth()->user()->can('course.view') || auth()->user()->can('exam.view') || auth()->user()->can('question_group.view'))
                <li class="dropdown {{ Request::route()->getName() == 'admin.course' ? 'active' : '' }} {{ Request::route()->getName() == 'admin.exam' ? 'active' : '' }} {{ Request::route()->getName() == 'admin.question_group' ? 'active' : '' }}">
                    <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cog"></i> <span>Soal</span></a>
                    <ul class="dropdown-menu">
                        @can('course.view')
                            <li class="{{ Request::route()->getName() == 'admin.course' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.course") }}">Mapel</a></li>
                        @endcan
                        @can('exam.view')
                            <li class="{{ Request::route()->getName() == 'admin.exam' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.exam") }}">Materi</a></li>
                        @endcan
                        @can('question_group.view')
                            <li class="{{ Request::route()->getName() == 'admin.question_group' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.question_group") }}">Grup Soal</a></li>
                        @endcan
                    </ul>
                </li>
            @endif
            @if(auth()->user()->can('vocational.view') || auth()->user()->can('program.view'))
                <li class="dropdown {{ Request::route()->getName() == 'admin.vocational' ? 'active' : '' }} {{ Request::route()->getName() == 'admin.program' ? 'active' : '' }}">
                    <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-stroopwafel"></i> <span>Kelas</span></a>
                    <ul class="dropdown-menu">
                        @can('vocational.view')
                            <li class="{{ Request::route()->getName() == 'admin.vocational' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.vocational") }}">Kejuruan</a></li>
                        @endcan
                        @can('program.view')
                            <li class="{{ Request::route()->getName() == 'admin.program' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.program") }}">Program</a></li>
                        @endcan
                    </ul>
                </li>
            @endif
            {{-- @can('user.view')
                <li class="{{ Request::route()->getName() == 'admin.vacational' ? 'active' : '' }}"><a href="{{ route("admin.vacational") }}" class="nav-link"><i class="fas fa-server"></i><span>Kejuruan</span></a></li>
            @endcan --}}
            @can('report.view')
                <li class="{{ Request::route()->getName() == 'admin.report' ? 'active' : '' }}"><a href="{{ route("admin.report") }}" class="nav-link"><i class="far fa-file-alt"></i><span>Laporan</span></a></li>
            @endcan
            <li class="menu-header">Lainnya</li>
            <li class="{{ Request::route()->getName() == 'admin.profile' ? 'active' : '' }}"><a href="{{ route("admin.profile") }}" class="nav-link"><i class="far fa-user"></i><span>Profil</span></a></li>
            @can('user.view')
                <li class="{{ Request::route()->getName() == 'admin.user' ? 'active' : '' }}"><a href="{{ route("admin.user") }}" class="nav-link"><i class="fas fa-users"></i><span>Pengguna</span></a></li>
            @endcan
            @if(auth()->user()->can('setting.view') || auth()->user()->can('permission.view') || auth()->user()->can('role.view'))
                <li class="dropdown {{ Request::route()->getName() == 'admin.setting' ? 'active' : '' }} {{ Request::route()->getName() == 'admin.permission' ? 'active' : '' }} {{ Request::route()->getName() == 'admin.role' ? 'active' : '' }}">
                    <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cog"></i> <span>Pengaturan</span></a>
                    <ul class="dropdown-menu">
                        @can('setting.view')
                            <li class="{{ Request::route()->getName() == 'admin.setting' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.setting") }}">Pengaturan Umum</a></li>
                        @endcan
                        @can('permission.view')
                            <li class="{{ Request::route()->getName() == 'admin.permission' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.permission") }}">Izin</a></li>
                        @endcan
                        @can('role.view')
                            <li class="{{ Request::route()->getName() == 'admin.role' ? 'active' : '' }}"><a class="nav-link" href="{{ route("admin.role") }}">Peran</a></li>
                        @endcan
                    </ul>
                </li>
            @endif
        </ul>
    </aside>
</div>
