<!DOCTYPE html>
<html lang="id">
    <head>
        @include('templates._partials._head')
        @include('templates._partials._styles_mail')
    </head>
    <body>
        <div id="app">
            <section class="section">
                @yield('content')
            </section>
        </div>
    </body>
</html>
