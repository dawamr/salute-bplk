@extends('templates.master')

@section('css-library')
<link rel="stylesheet" href="{{ asset('assets/modules/izitoast/css/iziToast.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>User</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route("dashboard") }}">Dashboard</a></div>
        <div class="breadcrumb-item">User</div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List User</h4>
                    @can ('user.create')
                        <div class="card-header-action">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="$('#modal-add-user').modal('show');" tooltip="Tambah User"><i class="fas fa-plus"></i> Tambah</a>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="user-list">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">
                                        #
                                    </th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th width="150">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@can ('user.manage')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-manage-user">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Manage User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-manage-user">
                    @csrf
                    @method('PUT')
                    <div class="modal-body" id="view-manage-user">

                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-manage-user">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('user.create')
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="javascript:void(0)" id="form-add-user">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Role</label>
                                <select name="role" id="add-user-role" class="form-control">
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" id="add-user-username" required autofocus>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="name" id="add-user-name" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" id="add-user-email" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" id="add-user-password" required>
                            </div>
                            <div class="form-group col-lg-6 col-md-12 col-sm-12">
                                <label>Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="add-user-password-confirm" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-add-user">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
@can ('user.update')
<div class="modal fade" tabindex="-1" role="dialog" id="modal-update-user">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="javascript:void(0)" id="form-update-user">
                @csrf
                @method('PUT')
                <input type="hidden" name="id" value="" id="update-user-id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Role</label>
                            <select name="role" id="update-user-role" class="form-control">
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" id="update-user-username" required autofocus>
                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" id="update-user-name" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" id="update-user-email" required>
                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" id="update-user-password" placeholder="Kosongkan password jika tidak ingin diubah">
                        </div>
                        <div class="form-group col-lg-6 col-md-12 col-sm-12">
                            <label>Konfirmasi Password</label>
                            <input type="password" class="form-control" name="password_confirmation" id="update-user-password-confirm">
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" id="btn-update-user">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endcan
@endsection

@section('js-library')
<script src="{{ asset('assets/modules/izitoast/js/iziToast.min.js') }}"></script>
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection

@section('js-script')
<script>

$(function () {
    "use strict";

    getUserList();

    @can ('user.manage')
    $("#form-manage-user").on("submit", function(e) {
        e.preventDefault();
        manageUser();
    });
    @endcan

    @can ('user.create')
    $("#form-add-user").on("submit", function(e) {
        e.preventDefault();
        addUser();
    });
    @endcan

    @can ('user.update')
    $("#form-update-user").on("submit", function(e) {
        e.preventDefault();
        updateUser();
    });
    @endcan
});

async function getUserList()
{
    $("#user-list").dataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.all') }}",
        destroy: true,
        columns: [
            { data: 'id' },
            { data: 'username' },
            { data: 'name' },
            { data: 'email' },
            { data: 'role' },
            { data: 'action' },
        ]
    });
}

@can('user.manage')
async function getManageUser(obj)
{
    var id = $(obj).data('id');
    $('#modal-manage-user').modal('show');
    $("#view-manage-user").html('<h4 class="text-center my-4">Loading . . .</h4>');

    $.ajax({
        url: "{{ route('user.manage.data') }}",
        type: "POST",
        dataType: "html",
        data: {
            "id": id,
            "_method": "POST",
            "_token": "{{ csrf_token() }}"
        },
        beforeSend() {
            $("#btn-manage-user").addClass('btn-progress');
            $("#btn-manage-user").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-manage-user").removeAttr('disabled', 'disabled');
            $("#btn-manage-user").removeClass('btn-progress');
        },
        success : function(result) {
            $("#view-manage-user").html(result);
        }
    });
}
async function manageUser()
{
    var formData = $("#form-manage-user").serialize();

    $.ajax({
        url: "{{ route('user.manage') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("#btn-manage-user").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("#btn-manage-user").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-manage-user").removeAttr('disabled', 'disabled');
            $("#btn-manage-user").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-manage-user")[0].reset();
                $('#modal-manage-user').modal('hide');
                getUserList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can ('user.create')
async function addUser()
{
    var formData = $("#form-add-user").serialize();

    $.ajax({
        url: "{{ route('user.store') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("#btn-add-user").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("select").attr('disabled', 'disabled');
            $("#btn-add-user").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("select").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-add-user").removeAttr('disabled', 'disabled');
            $("#btn-add-user").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-add-user")[0].reset();
                $('#modal-add-user').modal('hide');
                getUserList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can ('user.update')
async function editUser(obj)
{
    var id = $(obj).data('id');
    $('#modal-update-user').modal('show');
    $('#form-update-user')[0].reset();

    $.ajax({
        url: "{{ route('user.edit') }}",
        type: "POST",
        dataType: "json",
        data: {
            "id": id,
            "_method": "POST",
            "_token": "{{ csrf_token() }}"
        },
        beforeSend() {
            $("#btn-update-user").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("select").attr('disabled', 'disabled');
            $("#btn-update-user").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("select").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-user").removeAttr('disabled', 'disabled');
            $("#btn-update-user").removeClass('btn-progress');
        },
        success : function(result) {
            $('#update-user-id').val(result['data']['user']['id']);
            $('#update-user-username').val(result['data']['user']['username']);
            $('#update-user-name').val(result['data']['user']['name']);
            $('#update-user-email').val(result['data']['user']['email']);
            $('#update-user-role').val(result['data']['role']['id']);
        }
    });
}

async function updateUser()
{
    var formData = $("#form-update-user").serialize();

    $.ajax({
        url: "{{ route('user.update') }}",
        type: "POST",
        dataType: "json",
        data: formData,
        beforeSend() {
            $("#btn-update-user").addClass('btn-progress');
            $("input").attr('disabled', 'disabled');
            $("select").attr('disabled', 'disabled');
            $("#btn-update-user").attr('disabled', 'disabled');
            $("button").attr('disabled', 'disabled');
        },
        complete() {
            $("input").removeAttr('disabled', 'disabled');
            $("select").removeAttr('disabled', 'disabled');
            $("button").removeAttr('disabled', 'disabled');
            $("#btn-update-user").removeAttr('disabled', 'disabled');
            $("#btn-update-user").removeClass('btn-progress');
        },
        success : function(result) {
            if(result['status'] == 'success'){
                $("#form-update-user")[0].reset();
                $('#modal-update-user').modal('hide');
                getUserList();
            }

            notification(result['status'], result['title'], result['msg']);
        }
    });
}
@endcan

@can('user.delete')
async function deleteUser(object)
{
    var id = $(object).data('id');
    swal({
        title: 'Anda yakin?',
        text: 'Setelah dihapus, Anda tidak dapat memulihkannya kembali',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            swal({
                title: 'Loading..!',
                text: 'Tunggu sebentar..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                buttons:false,
            })
            $.ajax({
                url: "{{ route('user.destroy') }}",
                type: "POST",
                dataType: "json",
                data: {
                    "id": id,
                    "_method": "DELETE",
                    "_token": "{{ csrf_token() }}"
                },
                beforeSend() {

                },
                complete() {
                },
                success : function(result) {
                    if(result['status'] == 'success'){
                        getUserList();
                    }
                    swalNotification(result['status'], result['msg']);
                }
            });
        } else {
            swal('Data Anda Aman!', {icon:'success'});
        }
    });

}
@endcan

async function swalNotification(status, message)
{
    swal(message, {
        icon: status,
    });
}

async function notification(status, titleToast, messageToast)
{
    if(status == 'success'){
        iziToast.success({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'warning'){
        iziToast.warning({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }
    if(status == 'error'){
        iziToast.error({
            title: titleToast,
            message: messageToast,
            position: 'topRight'
        });
    }

}

</script>
@endsection

