<?php

/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
|
| You can update this public route
|
*/

Route::get('/', 'DashboardController@index')->name('admin.dashboard');

// training
Route::group(['prefix' => 'trainings'], function() {
    Route::get('/', 'TrainingController@index')->name('admin.training');

    Route::get('/data/peserta/{training?}', 'TrainingController@dataParticipant')->name('admin.training.data.participant');
    Route::get('/data/pengajar/{training?}', 'TrainingController@dataInstructor')->name('admin.training.data.instructor');
    Route::get('/data/mapel/{training?}', 'TrainingController@dataCourse')->name('admin.training.data.course');
    Route::get('/data', 'TrainingController@data')->name('admin.training.data');

    Route::get('/data/select2/peserta/{training?}', 'TrainingController@listParticipant')->name('admin.training.data.select2.participant');
    Route::get('/data/select2/pengajar/{training?}', 'TrainingController@listInstructor')->name('admin.training.data.select2.instructor');
    Route::get('/data/select2/mapel/{training?}', 'TrainingController@listCourse')->name('admin.training.data.select2.course');

    Route::get('/{training?}', 'TrainingController@show')->name('admin.training.show');

    Route::post('/', 'TrainingController@store')->name('admin.training.store');
    Route::post('/update', 'TrainingController@edit')->name('admin.training.edit');
    Route::post('/{training?}/peserta', 'TrainingController@storeParticipant')->name('admin.training.store.participant');
    Route::post('/{training?}/pengajar', 'TrainingController@storeInstructor')->name('admin.training.store.instructor');
    Route::post('/{training?}/mapel', 'TrainingController@storeCourse')->name('admin.training.store.course');

    Route::put('/update', 'TrainingController@update')->name('admin.training.update');

    Route::delete('/', 'TrainingController@destroy')->name('admin.training.destroy');
});

// qestion
Route::group(['prefix' => 'questions'], function() {
    Route::get('/', 'QuestionController@index')->name('admin.question');
    Route::get('/data/{exam?}', 'QuestionController@data')->name('admin.question.data');
    Route::get('/{course}', 'QuestionController@show')->name('admin.question.show');

    Route::post('/', 'QuestionController@store')->name('admin.question.store');
    Route::post('/update', 'QuestionController@edit')->name('admin.question.edit');

    Route::put('/update', 'QuestionController@update')->name('admin.question.update');

    Route::delete('/', 'QuestionController@destroy')->name('admin.question.destroy');
});

/*
|--------------------------------------------------------------------------
| Data Routes
|--------------------------------------------------------------------------
|
| All your data save in this route
|
*/

// participant
Route::group(['prefix' => 'participants'], function() {
    Route::get('/', 'ParticipantController@index')->name('admin.participant');
    Route::get('/data', 'ParticipantController@data')->name('admin.participant.data');

    Route::post('/', 'ParticipantController@store')->name('admin.participant.store');
    Route::post('/update', 'ParticipantController@edit')->name('admin.participant.edit');

    Route::put('/update', 'ParticipantController@update')->name('admin.participant.update');

    Route::delete('/', 'ParticipantController@destroy')->name('admin.participant.destroy');
});

// instructor
Route::group(['prefix' => 'instructors'], function() {
    Route::get('/', 'InstructorController@index')->name('admin.instructor');
    Route::get('/data', 'InstructorController@data')->name('admin.instructor.data');

    Route::post('/', 'InstructorController@store')->name('admin.instructor.store');
    Route::post('/update', 'InstructorController@edit')->name('admin.instructor.edit');

    Route::put('/update', 'InstructorController@update')->name('admin.instructor.update');

    Route::delete('/', 'InstructorController@destroy')->name('admin.instructor.destroy');
});

// course
Route::group(['prefix' => 'courses'], function() {
    Route::get('/', 'CourseController@index')->name('admin.course');
    Route::get('/data/select2', 'CourseController@list')->name('admin.course.data.select2');
    Route::get('/data/question', 'CourseController@listQuestion')->name('admin.course.data.question');
    Route::get('/data', 'CourseController@data')->name('admin.course.data');

    Route::post('/', 'CourseController@store')->name('admin.course.store');
    Route::post('/update', 'CourseController@edit')->name('admin.course.edit');

    Route::put('/update', 'CourseController@update')->name('admin.course.update');

    Route::delete('/', 'CourseController@destroy')->name('admin.course.destroy');

});

// exam
Route::group(['prefix' => 'exams'], function() {
    Route::get('/', 'ExamController@index')->name('admin.exam');
    Route::get('/data/select2', 'ExamController@list')->name('admin.exam.data.select2');
    Route::get('/data/{course?}', 'ExamController@data')->name('admin.exam.data');

    Route::post('/', 'ExamController@store')->name('admin.exam.store');
    Route::post('/update', 'ExamController@edit')->name('admin.exam.edit');

    Route::put('/update', 'ExamController@update')->name('admin.exam.update');

    Route::delete('/', 'ExamController@destroy')->name('admin.exam.destroy');

});

// question_group
Route::group(['prefix' => 'question-groups'], function() {
    Route::get('/', 'QuestionGroupController@index')->name('admin.question_group');
    Route::get('/data/select2/{exam?}', 'QuestionGroupController@list')->name('admin.question_group.data.select2');
    Route::get('/data/{exam?}', 'QuestionGroupController@data')->name('admin.question_group.data');

    Route::post('/', 'QuestionGroupController@store')->name('admin.question_group.store');
    Route::post('/update', 'QuestionGroupController@edit')->name('admin.question_group.edit');

    Route::put('/update', 'QuestionGroupController@update')->name('admin.question_group.update');

    Route::delete('/', 'QuestionGroupController@destroy')->name('admin.question_group.destroy');
});

// vocational
Route::group(['prefix' => 'vocationals'], function() {
    Route::get('/', 'VocationalController@index')->name('admin.vocational');
    Route::get('/data/select2', 'VocationalController@list')->name('admin.vocational.data.select2');
    Route::get('/data', 'VocationalController@data')->name('admin.vocational.data');

    Route::post('/', 'VocationalController@store')->name('admin.vocational.store');
    Route::post('/update', 'VocationalController@edit')->name('admin.vocational.edit');

    Route::put('/update', 'VocationalController@update')->name('admin.vocational.update');

    Route::delete('/', 'VocationalController@destroy')->name('admin.vocational.destroy');

});

// program
Route::group(['prefix' => 'programs'], function() {
    Route::get('/', 'ProgramController@index')->name('admin.program');
    Route::get('/data/select2/{vocational?}', 'ProgramController@list')->name('admin.program.data.select2');
    Route::get('/data/{vocational?}', 'ProgramController@data')->name('admin.program.data');

    Route::post('/', 'ProgramController@store')->name('admin.program.store');
    Route::post('/update', 'ProgramController@edit')->name('admin.program.edit');

    Route::put('/update', 'ProgramController@update')->name('admin.program.update');

    Route::delete('/', 'ProgramController@destroy')->name('admin.program.destroy');
});

// report
Route::group(['prefix' => 'reports'], function() {
    Route::get('/', 'QuestionGroupController@index')->name('admin.report');
});


/*
|--------------------------------------------------------------------------
| Core Routes
|--------------------------------------------------------------------------
|
| Please don't change this route
|
*/

// profile
Route::group(['prefix' => 'profile'], function() {
    Route::get('/', 'ProfileController@index')->name('admin.profile');
    Route::put('/', 'ProfileController@update')->name('admin.profile.update')->middleware('ajax');
});

// users
Route::group(['prefix' => 'users'], function() {
    Route::get('/', 'UserController@index')->name('admin.user');
    Route::get('/data', 'UserController@data')->name('admin.user.data');

    Route::post('/', 'UserController@store')->name('admin.user.store');
    Route::post('/update', 'UserController@edit')->name('admin.user.edit');
    Route::post('/manage/get', 'UserController@getManage')->name('admin.user.manage.get');

    Route::put('/update', 'UserController@update')->name('admin.user.update');
    Route::put('/manage', 'UserController@manage')->name('admin.user.manage');

    Route::delete('/', 'UserController@destroy')->name('admin.user.destroy');
});

// settings
Route::group(['prefix' => 'settings'], function() {
    Route::get('/', 'SettingController@index')->name('admin.setting');
});

// permissions
Route::group(['prefix' => 'permissions'], function() {
    Route::get('/', 'PermissionController@index')->name('admin.permission');
    Route::get('/data', 'PermissionController@data')->name('admin.permission.data');

    Route::post('/', 'PermissionController@store')->name('admin.permission.store');
    Route::post('/update', 'PermissionController@edit')->name('admin.permission.edit');

    Route::put('/update', 'PermissionController@update')->name('admin.permission.update');

    Route::delete('/', 'PermissionController@destroy')->name('admin.permission.destroy');
});

// roles
Route::group(['prefix' => 'roles'], function() {
    Route::get('/', 'RoleController@index')->name('admin.role');
    Route::get('/data', 'RoleController@data')->name('admin.role.data');

    Route::post('/', 'RoleController@store')->name('admin.role.store');
    Route::post('/update', 'RoleController@edit')->name('admin.role.edit');
    Route::post('/manage/get', 'RoleController@getManage')->name('admin.role.manage.get');
    Route::post('/default-user', 'RoleController@setDefault')->name('admin.role.default');

    Route::put('/update', 'RoleController@update')->name('admin.role.update');
    Route::put('/manage', 'RoleController@manage')->name('admin.role.manage');

    Route::delete('/', 'RoleController@destroy')->name('admin.role.destroy');
});
